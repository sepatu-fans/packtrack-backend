# packtrack-backend

**Install**

- `git clone [repo-url]`
- `cd [project-path]`
- `npm install --verbose`

**How to Running**
- Modify env file
- `cp .env.sample .env`
- `nano .env`
- set db configuration
- `node server.js`

**Auto Start**
- Install forever (`npm -g install forever`)
- `cd [project-path]`
- `forever start server.js`
