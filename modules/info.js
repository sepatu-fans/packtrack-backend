module.exports = function(app,connection){
    app.get('/api/info', (req, res) => {
        return res.json({
            status:true, 
            data: {
                last_commit_id:process.env.LAST_COMMIT_ID,
                last_commit_date:process.env.LAST_COMMIT_DATE
            }
        });
    })

    app.post('/api/log', (req, res) => {
        var sql = "INSERT INTO logs (user,log,created_at,created_by) values ?";
        var values = [];        
        values.push([req.body.user, req.body.log, req.body.created_at, req.body.created_by]);
        connection.query(sql, [values], function (err, result) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                console.log(result);
                return res.json({ success: true, data: result.insertId});
            }
        })
    })

    app.get('/api/label-logs', (req, res) => {
        var id = req.query.id;
        var sql = "SELECT ifnull(sc.convertions, log.type) as type, concat(l.nama, ' - ', s.nama) as lokasi_info, ";
        sql     += " if(li.id is not null, ifnull(g.nama, 'Grade A'), ";
        sql     += "    if(ga.id is not null, concat('Grade A - ', lo.serial_no), ";
        sql     += "        if(go.id is not null, concat('Grade Other - ', lo.serial_no), ";
        sql     += "            if(ship.id is not null, concat(ifnull(g.nama, 'Grade A'), ' - ', ship.nama_toko, ' - ', ship.no_so),  ";
        sql     += "                if(recv.id is not null, concat(ifnull(g.nama, 'Grade A'), ' - ', recv.nama_toko, ' - ', recv.no_so),  ";
        sql     += "                    if(trf.id is not null, concat(ifnull(g.nama, 'Grade A'), ' - ', trf.no_so), '')";
        sql     += "                ) ";
        sql     += "            ) ";
        sql     += "        ) ";
        sql     += "    ) ";
        sql     += " )	as info, log.created_at, u.username ";
        sql     += " FROM labels_inner_log log ";
        sql     += " left join vw_innerbox_info info on info.innerbox = log.innerbox";
        sql     += " left join location l on log.lokasi = l.id";
        sql     += " left join site s on l.site = s.id";
        sql     += " LEFT JOIN labels_inner li on log.type in ('NOBOX') and log.innerbox = li.id";
        sql     += " LEFT JOIN grades_a ga on log.type in ('GRADE-A') and log.reffid = ga.id";
        sql     += " LEFT JOIN grades_other go on log.type in ('GRADE-OTHER') and log.reffid = go.id";
        sql     += " LEFT JOIN labels_outer lo on lo.id = ifnull(ga.outerbox, go.outerbox)";
        sql     += " LEFT JOIN shipping ship on log.type in ('SHIPPING','SHIPPING-NOBOX','SHIPPING-VERIFIED') and log.reffid = ship.id";
        sql     += " LEFT JOIN receiving recv on log.type in ('RECEIVING','RECEIVING-NOBOX','RECEIVING-VERIFIED') and log.reffid = recv.id";
        sql     += " LEFT JOIN transfer trf on log.type in ('TRANSFER','TRANSFER-NOBOX','TRANSFER-VERIFIED') and log.reffid = trf.id ";
        sql     += " LEFT JOIN status_convertions sc on sc.status = log.type";
        sql     += " LEFT JOIN grades g on log.grade = g.id"
        sql     += " LEFT JOIN user u on log.created_by = u.id"
        sql     += " where log.innerbox = " + id;
        sql     += " order by log.created_at desc";

        connection.query(sql, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                return res.json({status:true, data:rows});
            }
        });
    })

    app.get('/api/label-outer-logs', (req, res) => {
        var id = req.query.id;
        var sql = "SELECT ifnull(sc.convertions, log.type) as type, concat(l.nama, ' - ', s.nama) as lokasi_info, ";
        sql     += " if(ga.id is not null, concat('Grade A - ', lo.serial_no), ";
        sql     += "         if(go.id is not null, concat('Grade Other - ', lo.serial_no), ";
        sql     += "            if(ship.id is not null, concat(ifnull(g.nama, 'Grade A'), ' - ', ship.nama_toko, ' - ', ship.no_so),  ";
        sql     += "                if(recv.id is not null, concat(ifnull(g.nama, 'Grade A'), ' - ', recv.nama_toko, ' - ', recv.no_so),  ";
        sql     += "                    if(trf.id is not null, concat(ifnull(g.nama, 'Grade A'), ' - ', trf.no_so), '')";
        sql     += "                ) ";
        sql     += "            ) ";
        sql     += "         ) ";
        sql     += " )	as info, log.created_at, u.username ";
        sql     += " FROM labels_outer_log log ";
        sql     += " left join location l on log.lokasi = l.id";
        sql     += " left join site s on l.site = s.id";
        sql     += " LEFT JOIN grades_a ga on log.type in ('GRADE-A') and log.reffid = ga.id ";
        sql     += " LEFT JOIN grades_other go on log.type in ('GRADE-OTHER') and log.reffid = go.id ";
        sql     += " LEFT JOIN labels_outer lo on lo.id = ifnull(ga.outerbox, go.outerbox) ";
        sql     += " LEFT JOIN shipping ship on log.type in ('SHIPPING','SHIPPING-NOBOX','SHIPPING-VERIFIED') and log.reffid = ship.id ";
        sql     += " LEFT JOIN receiving recv on log.type in ('RECEIVING','RECEIVING-NOBOX','RECEIVING-VERIFIED') and log.reffid = recv.id ";
        sql     += " LEFT JOIN transfer trf on log.type in ('TRANSFER','TRANSFER-NOBOX','TRANSFER-VERIFIED') and log.reffid = trf.id ";
        sql     += " LEFT JOIN status_convertions sc on sc.status = log.type";
        sql     += " LEFT JOIN grades g on log.grade = g.id"
        sql     += " LEFT JOIN user u on log.created_by = u.id"
        sql     += " where log.outerbox = " + id;
        sql     += " order by log.created_at desc";

        connection.query(sql, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                return res.json({status:true, data:rows});
            }
        });
    })
}