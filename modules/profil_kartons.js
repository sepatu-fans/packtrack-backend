module.exports = function(app,connection){

    app.get('/api/profil-karton', (req, res) => {
        var size = 10;
        if(req.query.size) {
            size = parseInt(req.query.size);
        }
        var limit = 0;
        var page = 1;
        var search = "";
        if(req.query.search) {
            search = req.query.search;
        }
        if(req.query.page && req.query.page != "null") {
            page = parseInt(req.query.page);
            limit = (page - 1) * size;
        }
        connection.query('SELECT * from profil_karton where (nama like "%' + search + '%" or if(tipe = 1, "Solid", "Asort") like "%' + search + '%") limit ' + limit + ',' + size, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                connection.query('SELECT count(*) as total from profil_karton where (nama like "%' + search + '%" or if(tipe = 1, "Solid", "Asort") like "%' + search + '%")', function (err1, rows1, fields1) {
                    if (err1)  {
                        return res.json({status:false, error:'Terjadi kesalahan. Err :' + JSON.stringify(err1)});
                    }
                    else {
                        var total = rows1[0].total;
                        var maxPage = Math.ceil( total / size )
                        var pages = [];
                        if(page%5==0 && page > 0) {
                            page = page-1;
                        }
                        for(var i = (page-(page%5)+1); i <= maxPage && i <= (page-(page%5)+5); i++) {
                            pages.push(i);
                        }
                        if(pages.length == 0) {
                            pages.push(1);
                        }
                        return res.json({
                            'data' : rows,
                            'pages' : pages
                        })
                    }
                })
            }
        })
    })

    app.get('/api/profil-karton-by-id', (req, res) => {
        var id = req.query.id;
        connection.query('SELECT a.* from profil_karton a where id = ' + id + '', function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    var profil_karton = rows[0];
                    if(profil_karton.tipe === 3) {
                        var plu = profil_karton.plu;
                        var plus = JSON.parse(plu);
                        var itemid = "";
                        plus.forEach((e) => {
                            if(itemid !== '') {
                                itemid += ',';
                            }
                            itemid += e[0];
                        })
                        connection.query('SELECT a.* from items a where id in (' + itemid + ')', function (err, rows, fields) {
                            if (err)  {
                                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                            }
                            else {
                                if(rows.length == 0) {
                                    return res.json({status:true, data:profil_karton, details:[]});
                                }else {
                                    return res.json({status:true, data:profil_karton, details:rows});
                                }
                            }
                        })
                    }else  if(profil_karton.tipe === 2) {
                        var size = profil_karton.size;
                        var sizes = JSON.parse(size);
                        var itemid = "";
                        sizes.forEach((e) => {
                            if(itemid !== '') {
                                itemid += ',';
                            }
                            itemid += e[0];
                        })
                        return res.json({status:true, data:profil_karton});
                    }else {
                        return res.json({status:true, data:profil_karton, details:[]});
                    }
                }
            }
        })
    })

    app.post('/api/profil-karton-add', (req, res) => {
        
        if(req.body.id === undefined) {
            var sql = "INSERT INTO profil_karton (nama,plu,size,tipe,sku,jumlah,created_at,created_by) values ?";
            var values = [];
            values.push([req.body.nama, req.body.plu, req.body.size, req.body.tipe, req.body.sku, req.body.jumlah, req.body.created_at, req.body.created_by]);
            connection.query(sql, [values], function (err, result) {
                if (err)  {
                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                }
                else {
                    console.log(result);
                    return res.json({ status: true, data: result.insertId});
                }
            })
        }else {
            connection.query("update profil_karton set nama = '" + req.body.nama + "', plu = '" + req.body.plu + "', size = '" + req.body.size + "', sku = '" + req.body.sku + "', jumlah = '" + req.body.jumlah + "' where id =" + req.body.id, function (err, result) {
                if (err)  {
                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                }
                else {
                    console.log(result);
                    return res.json({ status: true, data: result.insertId});
                }
            })
        }
    })

    app.post('/api/profil-karton-delete', (req, res) => {
        
        var sql = "DELETE FROM profil_karton where id = " + req.body.id;
        connection.query(sql, function (err, result) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                console.log(result);
                return res.json({ status: true, data: result.insertId});
            }
        })
    })

    app.get('/api/profil-karton-list', (req, res) => {
        connection.query('SELECT * from profil_karton', function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    return res.json({status:true, data:rows});
                }
            }
        })
    })

}