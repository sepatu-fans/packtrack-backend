module.exports = function(app,connection){

    app.get('/api/dashboard/shipping', (req, res) => {
        var lokasi = 0
        if(req.query.lokasi) {
            lokasi = req.query.lokasi;
        }
        connection.query('SELECT count(*) as total from shipping a where a.lokasi = ' + lokasi + '', function (err1, rows1, fields1) {
            if (err1)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + JSON.stringify(err1)});
            }
            else {
                var total = rows1[0].total;
                return res.json({
                    'data' : total
                })
            }
        })
    })

    app.get('/api/dashboard/item', (req, res) => {
        connection.query('SELECT count(*) as total from items a', function (err1, rows1, fields1) {
            if (err1)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + JSON.stringify(err1)});
            }
            else {
                var total = rows1[0].total;
                return res.json({
                    'data' : total
                })
            }
        })
    })

}