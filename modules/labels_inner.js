const json2csv = require('json2csv');
module.exports = function(app,connection){

    app.get('/api/labels-inner', (req, res) => {
        var size = 10;
        if(req.query.size) {
            size = parseInt(req.query.size);
        }
        var limit = 0;
        var page = 1;
        var search = "";
        if(req.query.search) {
            search = req.query.search;
        }
        if(req.query.page && req.query.page != "null") {
            page = parseInt(req.query.page);
            limit = (page - 1) * size;
        }
        var sql     = 'SELECT '
        sql         += '    a.*, '
        sql         += '    b.code, '
        sql         += '    b.name, '
        sql         += '    c.nama as color, '
        sql         += '    b.size, '
        sql         += '    ifnull(s.cm,0) as cm, '
        sql         += '    ifnull(s.us,0) as us, '
        sql         += '    ifnull(s.uk, 0) as uk, '
        sql         += '    b.barcode, '
        sql         += '    concat(log.type, if(log.info != "", " - ", ""), log.info) as last_status '
        sql         += ' from labels_inner a'
        sql         += ' join items b on a.item = b.id '
        sql         += ' left join vw_innerbox_info log on log.innerbox = a.id '
        sql         += ' join color c on c.id = b.color '
        sql         += ' left join size s on s.size = b.size and s.manufaktur_id = b.manufacturer '
        sql         += ' where (b.name like "%' + search + '%" or b.color like "%' + search + '%" or b.barcode like "%' + search + '%" or a.serial_no like "%' + search + '%") '
        sql         += ' order by a.id desc limit ' + limit + ',' + size;
        
        connection.query(sql, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                connection.query('SELECT count(*) as total from labels_inner a join vw_items b on a.item = b.id where (b.name like "%' + search + '%" or b.color like "%' + search + '%" or b.barcode like "%' + search + '%" or a.serial_no like "%' + search + '%")', function (err1, rows1, fields1) {
                    if (err1)  {
                        return res.json({status:false, error:'Terjadi kesalahan. Err :' + JSON.stringify(err1)});
                    }
                    else {
                        var total = rows1[0].total;
                        var maxPage = Math.ceil(total / size);
                        var pages = [];
                        if(page%5==0 && page > 0) {
                            page = page-1;
                        }
                        for(var i = (page-(page%5)+1); i <= maxPage && i <= (page-(page%5)+5); i++) {
                            pages.push(i);
                        }
                        console.log(pages);
                        if(pages.length == 0) {
                            pages.push(1);
                        }
                        return res.json({
                            'data' : rows,
                            'pages' : pages
                        })
                    }
                })
            }
        })
    })

    app.get('/api/labels-inner-by-ids', (req, res) => {
        var id = "";
        if(Array.isArray(req.query.id)) {
            var ids = req.query.id;
            ids.forEach(e => {
                if(id == "") {
                    id = e;
                }else {
                    id += "," + e;
                }
            })
        }else {
            id = req.query.id;
        }
        var sql  = 'SELECT a.*, b.name, b.sku_name, c.nama as color, b.size, ifnull(s.cm,0) as cm, ifnull(s.us,0) as us, ifnull(s.uk,0) as uk, b.barcode, ifnull(g.nama, "Grade A") as grade '
        sql     += 'from labels_inner a '
        sql     += 'join items b on a.item = b.id '
        sql     += 'join color c on c.id = b.color '
        sql     += 'left join size s on s.size = b.size and s.manufaktur_id = b.manufacturer '
        sql     += 'left join vw_innerbox_logs clogs on clogs.innerbox = a.id '
        sql     += 'left join grades g on g.id = clogs.grade '
        sql     += 'where a.id in (' + id + ')';

        connection.query(sql, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    return res.json({status:true, data:rows});
                }
            }
        })
    })

    app.get('/api/labels-inner-by-id-min-max', (req, res) => {
        var idmin = req.query.idmin;
        var idmax = req.query.idmax;
        connection.query('SELECT a.*, b.name, b.sku_name, c.nama as color, b.size, ifnull(s.cm,0) as cm, ifnull(s.us,0) as us, ifnull(s.uk,0) as uk, b.barcode from labels_inner a join items b on a.item = b.id join color c on c.id = b.color left join size s on s.size = b.size and s.manufaktur_id = b.manufacturer where a.id between ' + idmin + ' and ' + idmax, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    return res.json({status:true, data:rows});
                }
            }
        })
    })

    app.get('/api/labels-inner-by-serialno-min-max', (req, res) => {
        var idmin = req.query.idmin;
        var idmax = req.query.idmax;
        var csv = req.query.csv;
        connection.query('SELECT id from labels_inner where serial_no="' + idmin + '"', function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    var datamin = rows[0];
                    connection.query('SELECT id from labels_inner where serial_no="' + idmax + '"', function (err, rows, fields) {
                        if (err)  {
                            return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                        }
                        else {
                            if(rows.length == 0) {
                                return res.json({status:false, error:'Data tidak ditemukan'});
                            }else {
                                var datamax = rows[0];
                                connection.query('SELECT a.*, b.name, b.sku_name, c.nama as color, b.size, ifnull(s.cm,0) as cm, ifnull(s.us,0) as us, ifnull(s.uk,0) as uk, b.barcode from labels_inner a join items b on a.item = b.id join color c on c.id = b.color left join size s on s.size = b.size and s.manufaktur_id = b.manufacturer where a.id between ' + datamin.id + ' and ' + datamax.id, function (err, rows, fields) {
                                    if (err)  {
                                        return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                                    }
                                    else {
                                        if(rows.length == 0) {
                                            return res.json({status:false, error:'Data tidak ditemukan'});
                                        }else {
                                            if(csv == "1") {
                                                var fields = [
                                                    {
                                                        label:"No Serial",
                                                        value:(row, field)=> row.serial_no
                                                    },
                                                    {
                                                        label:"Barcode",
                                                        value:(row, field)=> row.barcode
                                                    },
                                                    {
                                                        label:"SKU Name",
                                                        value:(row, field)=> row.sku_name
                                                    },
                                                    {
                                                        label:"Color",
                                                        value:(row, field)=> row.color
                                                    },
                                                    {
                                                        label:"Size",
                                                        value:(row, field)=> row.size
                                                    },
                                                    {
                                                        label:"CM",
                                                        value:(row, field)=> row.cm
                                                    },
                                                    {
                                                        label:"UK",
                                                        value:(row, field)=> row.uk
                                                    },
                                                    {
                                                        label:"US",
                                                        value:(row, field)=> row.us
                                                    },
                                                ]
                                                var title = "";
                                                if(rows.length == 1) {
                                                    title = rows[0].serial_no + ".csv";
                                                }else {
                                                    title = rows[0].serial_no + "-" + rows[rows.length-1].serial_no + ".csv";
                                                }
                                                csv = json2csv.parse(JSON.parse(JSON.stringify(rows)), {fields: fields});
                                                res.attachment(title);
                                                return res.status(200).send(csv);
                                            }
                                            return res.json({status:true, data:rows});
                                        }
                                    }
                                })
                            }
                        }
                    })
                }
            }
        })
    })

    app.get('/api/labels-inner-by-code', (req, res) => {
        var profil_karton = req.query.profil_karton;
        if(profil_karton !== undefined) {
            connection.query('SELECT a.* from profil_karton a where id = ' + profil_karton + '', function (err, rows, fields) {
                if (err)  {
                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                }
                else {
                    if(rows.length == 0) {
                        res.json({status:false, error:'Data tidak ditemukan'});
                    }else {
                        var profil_karton = rows[0];
                        if(profil_karton.tipe === 3) {
                            var plu = profil_karton.plu;
                            var plus = JSON.parse(plu);
                            var itemid = "";
                            plus.forEach((e) => {
                                if(itemid !== '') {
                                    itemid += ',';
                                }
                                itemid += e[0];
                            })
                            connection.query('SELECT b.*, a.id as innerbox, a.serial_no from labels_inner a join vw_items b on a.item = b.id where a.serial_no = "' + req.query.code + '" and b.id in (' + itemid + ')', function (err, rows, fields) {
                                if (err)  {
                                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                                }
                                else {
                                    if(rows.length == 0) {
                                        res.json({status:false, error:'Data tidak ditemukan'});
                                    }else {
                                        res.json({status:true, data:rows[0]});
                                    }
                                }
                            })
                        }else if(profil_karton.tipe == 2) {
                            var size = profil_karton.size;
                            var sizes = JSON.parse(size);
                            var itemid = "";
                            sizes.forEach((e) => {
                                if(itemid !== '') {
                                    itemid += ',';
                                }
                                itemid += e[0];
                            })
                            console.log(itemid);
                            connection.query("SELECT b.*, a.id as innerbox, a.serial_no from labels_inner a join vw_items b on a.item = b.id where a.serial_no = '" + req.query.code + "' and b.size in (" + itemid + ")", function (err, rows, fields) {
                                if (err)  {
                                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                                }
                                else {
                                    if(rows.length == 0) {
                                        res.json({status:false, error:'Data tidak ditemukan'});
                                    }else {
                                        res.json({status:true, data:rows[0]});
                                    }
                                }
                            })
                        }else {
                            connection.query("SELECT b.*, a.id as innerbox, a.serial_no from labels_inner a join vw_items b on a.item = b.id where a.serial_no = '" + req.query.code + "'", function (err, rows, fields) {
                                if (err)  {
                                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                                }
                                else {
                                    if(rows.length == 0) {
                                        res.json({status:false, error:'Data tidak ditemukan'});
                                    }else {
                                        res.json({status:true, data:rows[0]});
                                    }
                                }
                            })
                        }
                    }
                }
            })

        }else {
            var sql  = 'SELECT b.*, a.item, a.id as innerbox, a.serial_no, b.size, ifnull(s.cm,0) as cm, ifnull(s.us,0) as us, ifnull(s.uk,0) as uk, log.grade, ifnull(g.nama, "Grade A") as grade_name'
            sql     += ' from labels_inner a';
            sql     += ' join vw_items b on a.item = b.id';
            sql     += ' left join size s on s.size = b.size and s.manufaktur_id = b.manufacturer_id';
            sql     += ' left join vw_innerbox_logs log on log.innerbox = a.id'
            sql     += ' left join grades g on g.id = log.grade'
            sql     += ' where a.serial_no = "' + req.query.code + '"';

            connection.query(sql, function (err, rows, fields) {
                if (err)  {
                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                }
                else {
                    if(rows.length == 0) {
                        return res.json({status:false, error:'Data tidak ditemukan'});
                    }else {
                        return res.json({status:true, data:rows[0]});
                    }
                }
            })
        }
    })

    app.get('/api/print-labels-inner-by-ids', (req, res) => {
        var id = "";
        if(Array.isArray(req.query.id)) {
            var ids = req.query.id;
            ids.forEach(e => {
                if(id == "") {
                    id = e;
                }else {
                    id += "," + e;
                }
            })
        }else {
            id = req.query.id;
        }
        connection.query('update labels_inner set cetak_ke = cetak_ke + 1 where id in (' + id + ')', function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    return res.json({status:true, data:rows});
                }
            }
        })
    })

    app.get('/api/print-labels-inner-by-id-min-max', (req, res) => {
        var idmin = req.query.idmin;
        var idmax = req.query.idmax;
        connection.query('update labels_inner set cetak_ke = cetak_ke + 1 where id between ' + idmin + ' and ' + idmax, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    return res.json({status:true, data:rows});
                }
            }
        })
    })

    app.get('/api/print-labels-inner-by-serialno-min-max', (req, res) => {
        var idmin = req.query.idmin;
        var idmax = req.query.idmax;
        var csv = req.query.csv;
        connection.query('SELECT id from labels_inner where serial_no="' + idmin + '"', function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    var datamin = rows[0];
                    connection.query('SELECT id from labels_inner where serial_no="' + idmax + '"', function (err, rows, fields) {
                        if (err)  {
                            return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                        }
                        else {
                            if(rows.length == 0) {
                                return res.json({status:false, error:'Data tidak ditemukan'});
                            }else {
                                var datamax = rows[0];
                                connection.query('update labels_inner set cetak_ke = cetak_ke + 1 where id between ' + datamin.id + ' and ' + datamax.id, function (err, rows, fields) {
                                    if (err)  {
                                        return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                                    }
                                    else {
                                        if(rows.length == 0) {
                                            return res.json({status:false, error:'Data tidak ditemukan'});
                                        }else {
                                            return res.json({status:true, data:rows});
                                        }
                                    }
                                })
                            }
                        }
                    })
                }
            }
        })
    })

    app.get('/api/labels-inner-max-min', (req, res) => {
        connection.query('SELECT max(id) as max, min(id) as min from labels_inner', function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    var data = rows[0];
                    connection.query('SELECT serial_no from labels_inner where id=' + data.max, function (err, rows, fields) {
                        if (err)  {
                            return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                        }
                        else {
                            if(rows.length == 0) {
                                return res.json({status:false, error:'Data tidak ditemukan'});
                            }else {
                                var datatamax = rows[0];
                                connection.query('SELECT serial_no from labels_inner where id=' + data.min, function (err, rows, fields) {
                                    if (err)  {
                                        return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                                    }
                                    else {
                                        if(rows.length == 0) {
                                            return res.json({status:false, error:'Data tidak ditemukan'});
                                        }else {
                                            var datatamin = rows[0];
                                            return res.json({status:true, data:data, min:datatamin, max:datatamax});
                                        }
                                    }
                                })
                            }
                        }
                    })
                }
            }
        })
    })

    app.get('/api/labels-inner-get-new-code', (req, res) => {
        connection.query('SELECT get_serial_label(1) as serial_no', function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    return res.json({status:true, data:rows[0]});
                }
            }
        })
    })

    app.post('/api/labels-inner-add', (req, res) => {
        
        var sql = "INSERT INTO labels_inner (item,cetak_ke,created_at,created_by,serial_no) values ?";
        var values = [];
        for(var i = 0; i < parseInt(req.body.jumlah); i++) {
            values.push([req.body.item, 0, req.body.created_at, req.body.created_by, req.body.serial_no]);
        }
        connection.query(sql, [values], function (err, result) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                console.log(result);
                return res.json({ status: true, data: result.insertId});
            }
        })
    })

    app.post('/api/labels-inner-delete', (req, res) => {
        
        var sql = "DELETE FROM labels_inner where id = " + req.body.id;
        connection.query(sql, function (err, result) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                console.log(result);
                return res.json({ status: true, data: result.insertId});
            }
        })
    })
}