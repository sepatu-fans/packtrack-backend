
const json2csv = require('json2csv');

module.exports = function(app, connection){

    app.get('/api/items', (req, res) => {
        var size = 10;
        if(req.query.size) {
            size = parseInt(req.query.size);
        }
        var limit = 0;
        var page = 1;
        var search = "";
        if(req.query.search) {
            search = req.query.search;
        }
        if(req.query.page && req.query.page != "null") {
            page = parseInt(req.query.page);
            limit = (page - 1) * size;
        }

        var sql = 'SELECT i.*, c.nama as color, m.nama as manufacturer, get_category_kode(category_code,0) as category_code, size';
        var sqlFrom = ' from items i left join manufaktur m on i.manufacturer = m.id left join color c on i.color = c.id left join category k on k.id = i.category_code';
        var sqlWhere = ' WHERE (name like "%' + search + '%" or sku like "%' + search + '%" or barcode like "%' + search + '%" or c.nama like "%' + search + '%" or m.nama like "%' + search + '%" or get_category_kode(category_code,0) like "%' + search + '%")';
        if(req.query.status != '' && req.query.status !== undefined) {
            sqlWhere += ' and status = ' + req.query.status;
        }
        if(req.query.discontinue != '' && req.query.discontinue !== undefined) {
            sqlWhere += ' and discontinue = ' + req.query.discontinue;
        }
        var sqlLimit = ' limit ' + limit + ',' + size;
        
        connection.query(sql + sqlFrom + sqlWhere + sqlLimit, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                connection.query('SELECT count(*) as total ' + sqlFrom + sqlWhere, function (err1, rows1, fields1) {
                    if (err1)  {
                        return res.json({status:false, error:'Terjadi kesalahan. Err :' + JSON.stringify(err1)});
                    }
                    else {
                        var total = rows1[0].total;
                        var maxPage = Math.ceil( total / size )
                        var pages = [];
                        if(page%5==0 && page > 0) {
                            page = page-1;
                        }
                        for(var i = (page-(page%5)+1); i <= maxPage && i <= (page-(page%5)+5); i++) {
                            pages.push(i);
                        }
                        if(pages.length == 0) {
                            pages.push(1);
                        }
                        res.json({
                            'data' : rows,
                            'pages' : pages
                        })
                    }
                })
            }
        })
    })

    app.post("/api/items-uploadcsv", (req, res) => {
        var multer = require('multer');
        var path  = require('path');
        const storage = multer.diskStorage({
            destination : path.join('uploads/csv/'),
            filename: function(req, file, cb){
                cb(null, file.fieldname + '-' + Date.now() +
                path.extname(file.originalname));
            }
        });
    
        //init upload
        const upload = multer({
        storage : storage
        }).single('csv');
    
        upload(req, res, err => {
        if (err) return res.json({ status: false, error: "Terjadi kesalahan. Err " + err.sqlMessage});
        return res.json({ status: true, data:req.file.filename});
        });
    });
    
    app.post("/api/items-upload", (req, res) => {
        const csv = require('csv-parser');
        const fs = require('fs');
        var path  = require('path');
        const results = [];
    
        // console.log(req);
    
        fs.createReadStream(path.join('uploads/csv/' + req.body.csv) )
        .pipe(csv())
        .on('data', (row) => {
            results.push(row)
        })
        .on('end', () => {
            
            var values = [];
            results.forEach((e) => {
                values.push([
                    e['Item Code'],
                    e['Barcode'],
                    e['Item Name'],
                    e['Description'],
                    e['SKU'],
                    e['SKU Name'],
                    e['Sales Price'],
                    e['Unit Code'],
                    e['Kategori Code'],
                    e['Vendor Code'],
                    e['Vendor Item Code'],
                    e['Vendor Item Name'],
                    e['Manufacturer'],
                    e['Color'],
                    e['Size'],
                    e['Status'],
                    e['Discontinue'],
                    req.body.created_by
                ]);
            });

            var tmp = Math.random().toString(36).slice(2);
            var sql = "";
            values.forEach((e) => {
                sql += "select insert_item('" + e[0] + "','" + e[1] + "','" + e[2] + "','" + e[3] + "','" + e[4] + "','" + e[5] + "','" + e[6] + "','" + e[7] + "','" + e[8] + "','" + e[9] + "','" + e[10] + "','" + e[11] + "','" + e[12] + "','" + e[13] + "','" + e[14] + "','" + e[15] + "','" + e[16] + "'," + e[17] + ") as result;";
            });

            connection.query(sql, function (err, result) {
                if (err)  {
                    console.log(err);
                    return res.json({status:false, error:'Terjadi kesalahan. Data tidak bisa tersimpan.'});
                }
                else {
                    var msg = "";
                    var i = 0;
                    var s = 0;
                    result.forEach((e) => {
                        if(e[0].result == '') {
                            s++;
                        } else {
                            msg += values[i][1] + " gagal disimpan. " + e[0].result + "\n";
                        }
                        i++;
                    })
                    if(msg == '') {
                        return res.json({status:true, data:results.length});
                    } else {
                        msg = s + " data berhasil disimpan.\n\n" + msg;
                        return res.json({status:false, error:msg});
                    }
                }
            });
        });
    });

    app.get('/api/item-by-plu', (req, res) => {
        var barcode = "";
        if(req.query.barcode) {
            barcode = req.query.barcode;
        }
        connection.query('SELECT i.*, c.nama as color, s.cm, s.uk, s.us from items i left join color c on c.id = i.color left join size s on s.size = i.size and s.manufaktur_id = i.manufacturer where barcode = "' + barcode + '"', function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    return res.json({status:true, data:rows[0]});
                }
            }
        })
    });

    app.get('/api/item-by-id', (req, res) => {
        connection.query('SELECT * from items where id = "' + req.query.id + '"', function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    return res.json({status:true, data:rows[0]});
                }
            }
        })
    });

    app.post('/api/item-add', (req, res) => {
        
        if(req.body.id === undefined) {
            var sql = "INSERT INTO items (code,name,color,sku,sku_name,description,barcode,category_code,manufacturer,size,status,discontinue,created_at,created_by) values ?";
            var values = [];
            values.push([req.body.code,req.body.name,req.body.color,req.body.sku,req.body.sku_name,req.body.description,req.body.plu,req.body.category_code,req.body.manufakturer,req.body.size,req.body.status,req.body.discontinue,req.body.created_at,req.body.created_by]);
            connection.query(sql, [values], function (err, result) {
                if (err)  {
                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                }
                else {
                    console.log(result);
                    return res.json({ status: true, data: result.insertId});
                }
            })
        }else {
            connection.query("update items set code = '" + req.body.code + "',name = '" + req.body.name + "',color=" + req.body.color + ",sku='" + req.body.sku + "',sku_name='" + req.body.sku_name + "',description='" + req.body.description + "',barcode='" + req.body.plu + "',category_code=" + req.body.category_code + ',manufacturer=' + req.body.manufakturer + ',size=' + req.body.size + ",status=" + req.body.status + ",discontinue=" + req.body.discontinue + " where id =" + req.body.id, function (err, result) {
                if (err)  {
                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                }
                else {
                    console.log(result);
                    return res.json({ status: true, data: result.insertId});
                }
            })
        }
    })

    app.get('/api/item-download', (req, res) => {
        
        var sql = 'SELECT i.*, c.nama as color, m.nama as manufacturer, get_category_kode(category_code,0) as category_code';
        var sqlFrom = ' from items i left join manufaktur m on i.manufacturer = m.id left join color c on i.color = c.id left join category k on k.id = i.category_code';

        connection.query(sql + sqlFrom, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    console.log(rows);
                    var fields = [
                        {
                            label:"Item Code",
                            value:(row, field)=> row.code
                        },
                        {
                            label:"Barcode",
                            value:(row, field)=> row.barcode
                        },
                        {
                            label:"Item Name",
                            value:(row, field)=> row.name
                        },
                        {
                            label:"Description",
                            value:(row, field)=> row.description
                        },
                        {
                            label:"SKU",
                            value:(row, field)=> row.sku
                        },
                        {
                            label:"SKU Name",
                            value:(row, field)=> row.sku_name
                        },
                        {
                            label:"Sales Price",
                            value:(row, field)=> row.sales_price
                        },
                        {
                            label:"Unit Code",
                            value:(row, field)=> row.unit_code
                        },
                        {
                            label:"Kategori Code",
                            value:(row, field)=> row.category_code
                        },
                        {
                            label:"Vendor Code",
                            value:(row, field)=> row.vendor_code
                        },
                        {
                            label:"Vendor Item Code",
                            value:(row, field)=> row.vendor_item_code
                        },
                        {
                            label:"Vendor Item Name",
                            value:(row, field)=> row.vendor_item_name
                        },
                        {
                            label:"Manufacturer",
                            value:(row, field)=> row.manufacturer
                        },
                        {
                            label:"Color",
                            value:(row, field)=> row.color
                        },
                        {
                            label:"Size",
                            value:(row, field)=> row.size
                        },
                        {
                            label:"Status",
                            value:(row, field)=> row.status === 1 ? "TRUE" : "FALSE"
                        },
                        {
                            label:"Discontinue",
                            value:(row, field)=> row.discontinue === 1 ? "TRUE" : "FALSE"
                        },
                    ]
                    csv = json2csv.parse(JSON.parse(JSON.stringify(rows)), {fields: fields});
                    res.attachment("data-produk.csv");
                    return res.status(200).send(csv);
                }
            }
        })
    });

    app.post('/api/item-delete', (req, res) => {
        
        var sql = "DELETE FROM items where id = " + req.body.id;
        connection.query(sql, function (err, result) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                console.log(result);
                return res.json({ status: true, data: result.insertId});
            }
        })
    })

    app.get('/api/items-list', (req, res) => {
        connection.query('SELECT * from items order by name', function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    return res.json({status:true, data:rows});
                }
            }
        })
    })
}