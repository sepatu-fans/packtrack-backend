module.exports = function(app,connection){

    app.get('/api/prefix', (req, res) => {
        var size = 10;
        if(req.query.size) {
            size = parseInt(req.query.size);
        }
        var limit = 0;
        var page = 1;
        var search = "";
        if(req.query.search) {
            search = req.query.search;
        }
        if(req.query.page && req.query.page != "null") {
            page = parseInt(req.query.page);
            limit = (page - 1) * size;
        }
        connection.query('SELECT * from prefix where (prefix like "%' + search + '%") order by status desc limit ' + limit + ',' + size, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                connection.query('SELECT count(*) as total from prefix where (prefix like "%' + search + '%")', function (err1, rows1, fields1) {
                    if (err1)  {
                        return res.json({status:false, error:'Terjadi kesalahan. Err :' + JSON.stringify(err1)});
                    }
                    else {
                        var total = rows1[0].total;
                        var maxPage = Math.ceil( total / size )
                        var pages = [];
                        if(page%5==0 && page > 0) {
                            page = page-1;
                        }
                        for(var i = (page-(page%5)+1); i <= maxPage && i <= (page-(page%5)+5); i++) {
                            pages.push(i);
                        }
                        if(pages.length == 0) {
                            pages.push(1);
                        }
                        return res.json({
                            'data' : rows,
                            'pages' : pages
                        })
                    }
                })
            }
        })
    })

    app.post('/api/prefix-add', (req, res) => {
        
        if(req.body.id === undefined) {
            var sql = "INSERT INTO prefix (prefix,status,tipe,created_at,created_by) values ?";
            var values = [];
            values.push([req.body.prefix,req.body.status,req.body.tipe,req.body.created_at,req.body.created_by]);
            connection.query(sql, [values], function (err, result) {
                if (err)  {
                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                }
                else {
                    console.log(result);
                    var lastId = result.insertId;
                    if(req.body.status === "1") {
                        connection.query("update prefix set status = 0 where tipe =" + req.body.tipe + " and id != " + lastId, function (err, result) {
                            if (err)  {
                                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                            }
                            else {
                                console.log(result);
                                return res.json({ status: true, data: lastId});
                            }
                        })
                    }else {
                        return res.json({ status: true, data: result.insertId});
                    }
                }
            })
        }else {
            connection.query("update prefix set prefix = '" + req.body.prefix + "',status = '" + req.body.status + "',tipe = '" + req.body.tipe + "' where id =" + req.body.id, function (err, result) {
                if (err)  {
                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                }
                else {
                    console.log(req.body);
                    if(req.body.status === "1") {
                        connection.query("update prefix set status = 0 where tipe = " + req.body.tipe + " and id != " + req.body.id, function (err, result) {
                            if (err)  {
                                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                            }
                            else {
                                console.log(result);
                                return res.json({ status: true, data: req.body.id});
                            }
                        })
                    }else {
                        return res.json({ status: true, data: req.body.id});
                    }
                }
            })
        }
    })

    app.post('/api/prefix-delete', (req, res) => {
        
        var sql = "DELETE FROM prefix where id = " + req.body.id;
        connection.query(sql, function (err, result) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                console.log(result);
                return res.json({ status: true, data: result.insertId});
            }
        })
    })

    app.get('/api/prefix-list', (req, res) => {
        connection.query('SELECT * from prefix order by nama', function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    return res.json({status:true, data:rows});
                }
            }
        })
    })
    
    app.get('/api/prefix-by-id', (req, res) => {
        var id = req.query.id;
        connection.query('SELECT a.* from prefix a where id = ' + id + '', function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    return res.json({status:true, data:rows[0]});
                }
            }
        })
    })

}