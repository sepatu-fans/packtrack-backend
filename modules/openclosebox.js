module.exports = function(app,connection){
    
    app.get('/api/open-close-box', (req, res) => {
        var size = 10;
        if(req.query.size) {
            size = parseInt(req.query.size);
        }
        var limit = 0;
        var page = 1;
        var search = "";
        if(req.query.search) {
            search = req.query.search;
        }
        var lokasi = 0
        if(req.query.lokasi) {
            lokasi = req.query.lokasi;
        }
        var profil_karton = 0
        if(req.query.profil_karton) {
            profil_karton = req.query.profil_karton;
        }
        if(req.query.page && req.query.page != "null") {
            page = parseInt(req.query.page);
            limit = (page - 1) * size;
        }

        var sql = 'SELECT * from vw_open_close_box where (serial_no like "%' + search + '%" or info like "%' + search + '%" or color like "%' + search + '%" or profile like "%' + search + '%" or open_note like "%' + search + '%") and lokasi = ' + lokasi + ' order by created_at desc limit ' + limit + ',' + size;
        connection.query(sql, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                connection.query('SELECT count(*) as total from vw_open_close_box where (serial_no like "%' + search + '%" or info like "%' + search + '%" or color like "%' + search + '%" or profile like "%' + search + '%" or open_note like "%' + search + '%") and lokasi = ' + lokasi, function (err1, rows1, fields1) {
                    if (err1)  {
                        return res.json({status:false, error:'Terjadi kesalahan. Err :' + JSON.stringify(err1)});
                    }
                    else {
                        var total = rows1[0].total;
                        var maxPage = Math.ceil( total / size )
                        var pages = [];
                        if(page%5==0 && page > 0) {
                            page = page-1;
                        }
                        for(var i = (page-(page%5)+1); i <= maxPage && i <= (page-(page%5)+5); i++) {
                            pages.push(i);
                        }
                        if(pages.length == 0) {
                            pages.push(1);
                        }
                        return res.json({
                            'data' : rows,
                            'pages' : pages
                        })
                    }
                })
            }
        })
    })

    app.post('/api/open-close-box-open', (req, res) => {
        if(req.body.tipe === 'GRADE-A' || req.body.tipe === 'GRADE-A-CLOSE') {
            var sql = "UPDATE grades_a set is_close = 0, open_at = now(), close_at = null, open_note = '" + req.body.note + "', created_by = " + req.body.created_by + " where id = " + req.body.id + ";";
            connection.query(sql, function (err, result) {
                if (err)  {
                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                }
                else {
                    return res.json({ status: true});
                }
            })
        }else  {
            var sql = "UPDATE grades_other set is_close = 0, open_at = now(), close_at = null, open_note = '" + req.body.note + "', created_by = " + req.body.created_by + " where id = " + req.body.id + ";";
            connection.query(sql, function (err, result) {
                if (err)  {
                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                }
                else {
                    return res.json({ status: true});
                }
            })
        }
    })

    app.post('/api/open-close-box-close', (req, res) => {
        if(req.body.tipe === 'A') {
            var sql = 'SELECT * from grades_a_detail where status = 0 and grade_a = ' + req.body.id;
            connection.query(sql, function (err, rows, fields) {
                if (err)  {
                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                }else {
                    if(rows.length > 0) {
                        return res.json({ status: true, data: false});
                    }else {
                        var sql = "UPDATE grades_a set is_close = 1, close_at = now(), created_by = " + req.body.created_by + " where id = " + req.body.id + ";";
                        connection.query(sql, function (err, result) {
                            if (err)  {
                                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                            }
                            else {
                                return res.json({status: true, data: true});
                            }
                        })
                    }
                }
            });
        }else if(req.body.tipe === 'OTHER') {
            var sql = 'SELECT * from grades_other_detail where status = 0 and grade_other = ' + req.body.id;
            connection.query(sql, function (err, rows, fields) {
                if (err)  {
                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                }else {
                    if(rows.length > 0) {
                        return res.json({ status: true, data: false});
                    }else {
                        var sql = "UPDATE grades_other set is_close = 1, close_at = now(), created_by = " + req.body.created_by + " where id = " + req.body.id + ";";
                        connection.query(sql, function (err, result) {
                            if (err)  {
                                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                            }
                            else {
                                return res.json({ status: true, data: true});
                            }
                        })
                    }
                }
            });
        }
    })

    app.get('/api/open-close-box-check', (req, res) => {
        var sql  = 'SELECT * FROM vw_outerbox_logs';
        sql     += ' where serial_no = "' + req.query.outerbox + '"';
        sql     += ' and type in ("GRADE-A", "GRADE-A-CLOSE", "GRADE-OTHER", "GRADE-OTHER-CLOSE")';
        sql     += ' and lokasi = ' + req.query.lokasi;

        connection.query(sql, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length === 0) {
                    return res.json({status:false, error:'Label Outerbox tidak ditemukan, dilokasi berbeda atau sudah dalam kondisi terbuka'});
                }else {
                    return res.json({status:true, data:rows[0]});
                }
            }
        })
    })
}