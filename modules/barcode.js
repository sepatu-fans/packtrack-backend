const bwipjs = require('bwip-js');
const fs = require('fs')
const path = require('path')
const utils = require('util')
const puppeteer = require('puppeteer')
const hb = require('handlebars');
const { dir } = require('console');
const readFile = utils.promisify(fs.readFile)

module.exports = function(app,connection){
    bwipjs.loadFont('ARIAL', 100, 100, require('fs').readFileSync(__dirname + '/../Open Sans_regular.ttf', 'binary'));
    app.get('/api/barcode', (req, res) => {
        bwipjs.toBuffer({
            bcid: 'code128',
            text: req.query.code,
            scale: 7,
            height: req.query.height,
            includetext: true,
            textxalign: 'center',
            textfont: 'ARIAL',
            textsize: 11
        })
        .then(png => {
            // `png` is a Buffer as in the example above
            // res.writeHead(200, {
            //     'Content-Type': 'image/png',
            //     'Content-Length': png.length
            //   });
            res.send(png); // gives me 'Hello world' 
        })
        .catch(err => {
            // `err` may be a string or Error object
        });
    })

    app.get('/api/image', (req,res) => {
        res.sendFile(path.resolve(__dirname+"/../images/" + req.query.image))
    })

    async function getTemplateHtml(file) {

        console.log("Loading template file in memory")
        try {
            const labelPath = path.resolve(__dirname+"/../"+file);
            return await readFile(labelPath, 'utf8');
        } catch (err) {
            return Promise.reject("Could not load html template");
        }
    }
    
    
    async function generateInnerPdf(rows, res) {
    
        var data = [];
        var title = "";
        if(rows.length == 1) {
            title = rows[0].serial_no + ".pdf";
        }else {
            title = rows[0].serial_no + "-" + rows[rows.length-1].serial_no + ".pdf";
        }

        var port = process.env.PORT
        var default_path = process.env.DEFAULT_PATH

        rows.forEach((r) => {
            data.push({
                'sku_name':r.sku_name,
                'color':r.color,
                'cm':r.cm,
                'uk':r.uk,
                'us':r.us,
                'size':r.size,
                'barcode':'http://localhost:'+port+'/'+default_path+'/api/barcode?code='+r.barcode+'&height=18',
                'serial_no':'http://localhost:'+port+'/'+default_path+'/api/barcode?code='+r.serial_no+'&height=12',
                'cetak_ke':r.cetak_ke,
                'margin':data.length == 0 ? '-10':'-5'
            })
        })

        data = {
            data:data
        }
    
        getTemplateHtml("label_inner.html")
            .then(async (tmp) => {
                console.log("Compiing the template with handlebars")
                const template = hb.compile(tmp, { strict: true });
                // we have compile our code with handlebars
                const result = template(data);
                // We can use this to add dyamic data to our handlebas template at run time from database or API as per need. you can read the official doc to learn more https://handlebarsjs.com/
                const html = result;
    
                // we are using headless mode 
                const browser = await puppeteer.launch({
                    headless: true,
                    args: ['--no-sandbox']
                });
                const page = await browser.newPage()
    
                // We set the page content as the generated html by handlebars
                await page.setContent(html)
    
                // we Use pdf function to generate the pdf in the same folder as this file.
                await page.pdf({ path: 'inner.pdf', width: '74mm', height: '52mm',margin:{left:"1mm",top:"0mm"} })
    
                await browser.close();
                console.log("PDF Generated")

                res.download(path.resolve(__dirname+"/../inner.pdf"), title);
    
            })
            .catch(err => {
                console.error(err)
            });
    }

    async function generateOuterPdf(rows, res) {
    
        var data = [];
        var title = "";
        if(rows.length == 1) {
            title = rows[0].serial_no + ".pdf";
        }else {
            title = rows[0].serial_no + "-" + rows[rows.length-1].serial_no + ".pdf";
        }

        var port = process.env.PORT
        var default_path = process.env.DEFAULT_PATH

        rows.forEach((r) => {
            data.push({
                'serial_no':'http://localhost:'+port+'/'+default_path+'/api/barcode?code='+r.serial_no+'&height=22',
                'cetak_ke':r.cetak_ke,
                'logo_fans':'http://localhost:'+port+'/'+default_path+'/api/image?image=fans-logo.png',
                'logo_ili':'http://localhost:'+port+'/'+default_path+'/api/image?image=ili-logo.png',
                'margin':data.length == 0 ? '5':'7'
            })
        })

        data = {
            data:data
        }
    
        getTemplateHtml("label_outer.html")
            .then(async (tmp) => {
                console.log("Compiing the template with handlebars")
                const template = hb.compile(tmp, { strict: true });
                // we have compile our code with handlebars
                const result = template(data);
                // We can use this to add dyamic data to our handlebas template at run time from database or API as per need. you can read the official doc to learn more https://handlebarsjs.com/
                const html = result;
    
                // we are using headless mode 
                const browser = await puppeteer.launch({
                    headless: true,
                    args: ['--no-sandbox']
                });
                const page = await browser.newPage()
    
                // We set the page content as the generated html by handlebars
                await page.setContent(html)
    
                // we Use pdf function to generate the pdf in the same folder as this file.
                await page.pdf({ path: 'outer.pdf', width: '74mm', height: '52mm',margin:{left:"1mm",bottom:"0mm"}, pageRanges:"1-"+rows.length})
    
                await browser.close();
                console.log("PDF Generated")

                res.download(path.resolve(__dirname+"/../outer.pdf"),title);
    
            })
            .catch(err => {
                console.error(err)
            });
    }

    app.get('/api/barcode/label-inner-by-id-min-max', (req, res) => {
        var idmin = req.query.idmin;
        var idmax = req.query.idmax;
        connection.query('SELECT a.*, b.name, b.sku_name, c.nama as color, b.size, ifnull(s.cm,0) as cm, ifnull(s.us,0) as us, ifnull(s.uk,0) as uk, b.barcode from labels_inner a join items b on a.item = b.id join color c on c.id = b.color left join size s on s.size = b.size and s.manufaktur_id = b.manufacturer where a.id between ' + idmin + ' and ' + idmax, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    generateInnerPdf(rows, res);
                }
            }
        })
    });

    app.get('/api/barcode/label-inner-by-ids', (req, res) => {
        var id = "";
        if(Array.isArray(req.query.id)) {
            var ids = req.query.id;
            ids.forEach(e => {
                if(id == "") {
                    id = e;
                }else {
                    id += "," + e;
                }
            })
        }else {
            id = req.query.id;
        }
        connection.query('SELECT a.*, b.name, b.sku_name, c.nama as color, b.size, ifnull(s.cm,0) as cm, ifnull(s.us,0) as us, ifnull(s.uk,0) as uk, b.barcode from labels_inner a join items b on a.item = b.id join color c on c.id = b.color left join size s on s.size = b.size and s.manufaktur_id = b.manufacturer where a.id in (' + id + ')', function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    generateInnerPdf(rows, res);
                }
            }
        })
    });

    app.get('/api/barcode/label-inner-by-serialno-min-max', (req, res) => {
        var idmin = req.query.idmin;
        var idmax = req.query.idmax;
        connection.query('SELECT id from labels_inner where serial_no="' + idmin + '"', function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    var datamin = rows[0];
                    connection.query('SELECT id from labels_inner where serial_no="' + idmax + '"', function (err, rows, fields) {
                        if (err)  {
                            return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                        }
                        else {
                            if(rows.length == 0) {
                                return res.json({status:false, error:'Data tidak ditemukan'});
                            }else {
                                var datamax = rows[0];
                                connection.query('SELECT a.*, b.name, b.sku_name, c.nama as color, b.size, ifnull(s.cm,0) as cm, ifnull(s.us,0) as us, ifnull(s.uk,0) as uk, b.barcode from labels_inner a join items b on a.item = b.id join color c on c.id = b.color left join size s on s.size = b.size and s.manufaktur_id = b.manufacturer where a.id between ' + datamin.id + ' and ' + datamax.id, function (err, rows, fields) {
                                    if (err)  {
                                        return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                                    }
                                    else {
                                        if(rows.length == 0) {
                                            return res.json({status:false, error:'Data tidak ditemukan'});
                                        }else {
                                            generateInnerPdf(rows, res);
                                        }
                                    }
                                })
                            }
                        }
                    })
                }
            }
        })
    })

    app.get('/api/barcode/label-outer-by-ids', (req, res) => {
        var id = "";
        if(Array.isArray(req.query.id)) {
            var ids = req.query.id;
            ids.forEach(e => {
                if(id == "") {
                    id = e;
                }else {
                    id += "," + e;
                }
            })
        }else {
            id = req.query.id;
        }
        connection.query('SELECT a.* from labels_outer a where a.id in (' + id + ')', function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    generateOuterPdf(rows, res)
                }
            }
        })
    })

    app.get('/api/barcode/label-outer-by-id-min-max', (req, res) => {
        var idmin = req.query.idmin;
        var idmax = req.query.idmax;
        connection.query('SELECT a.* from labels_outer a where a.id between ' + idmin + ' and ' + idmax, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    generateOuterPdf(rows, res);
                }
            }
        })
    })

    app.get('/api/barcode/label-outer-by-serialno-min-max', (req, res) => {
        var idmin = req.query.idmin;
        var idmax = req.query.idmax;
        connection.query('SELECT id from labels_outer where serial_no="' + idmin + '"', function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    var datamin = rows[0];
                    connection.query('SELECT id from labels_outer where serial_no="' + idmax + '"', function (err, rows, fields) {
                        if (err)  {
                            return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                        }
                        else {
                            if(rows.length == 0) {
                                return res.json({status:false, error:'Data tidak ditemukan'});
                            }else {
                                var datamax = rows[0];
                                connection.query('SELECT a.* from labels_outer a where a.id between ' + datamin.id + ' and ' + datamax.id, function (err, rows, fields) {
                                    if (err)  {
                                        return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                                    }
                                    else {
                                        if(rows.length == 0) {
                                            return res.json({status:false, error:'Data tidak ditemukan'});
                                        }else {
                                            generateOuterPdf(rows, res);
                                        }
                                    }
                                })
                            }
                        }
                    })
                }
            }
        })
    })

}