const json2csv = require('json2csv');
module.exports = function(app,connection){

    app.get('/api/reports-ibx', (req, res) => {
        var size = 10;
        if(req.query.size) {
            size = parseInt(req.query.size);
        }
        var limit = 0;
        var page = 1;
        var search = "";
        if(req.query.search) {
            search = req.query.search;
        }
        if(req.query.page && req.query.page != "null") {
            page = parseInt(req.query.page);
            limit = (page - 1) * size;
        }
        var sql     = 'SELECT'
        sql         += '    li.serial_no,'
        sql         += '    i.barcode,'
        sql         += '    i.sku_name,'
        sql         += '    c.nama AS color,'
        sql         += '    s.size,'
        sql         += '    s.cm,'
        sql         += '    s.uk,'
        sql         += '    s.us,'
        sql         += '    gdi.serial_no as outerbox,'
        sql         += '    if(isnull(gdi.is_close), "", if(gdi.is_close = 1, "CLOSE", "OPEN")) as outerbox_status,'
        sql         += '    gdi.grade as grade,'
        sql         += '    st.nama as site,'
        sql         += '    l.nama as location,'
        sql         += '    ii.type,'
        sql         += '    ii.created_at as last_trans_date'
        sql         += ' FROM labels_inner li'
        sql         += ' JOIN items i ON li.item = i.id'
        sql         += ' JOIN size s ON i.size = s.size and s.manufaktur_id = i.manufacturer'
        sql         += ' JOIN color c ON i.color = c.id'
        sql         += ' LEFT JOIN vw_grades_detail_innerbox gdi on gdi.innerbox = li.id'
        sql         += ' LEFT JOIN vw_innerbox_info ii on ii.innerbox = li.id'
        sql         += ' LEFT JOIN location l on l.id = gdi.lokasi'
        sql         += ' LEFT JOIN site st on l.site = st.id'
        sql         += ' where (li.serial_no like "%' + search + '%" or i.barcode like "%' + search + '%" or i.sku_name like "%' + search + '%" or gdi.serial_no like "%' + search + '%") '
        sql         += ' order by li.id asc limit ' + limit + ',' + size;
        
        connection.query(sql, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                connection.query('SELECT count(*) as total from labels_inner a join vw_items b on a.item = b.id where (b.name like "%' + search + '%" or b.color like "%' + search + '%" or b.barcode like "%' + search + '%" or serial_no like "%' + search + '%")', function (err1, rows1, fields1) {
                    if (err1)  {
                        return res.json({status:false, error:'Terjadi kesalahan. Err :' + JSON.stringify(err1)});
                    }
                    else {
                        var total = rows1[0].total;
                        var maxPage = Math.ceil(total / size);
                        var pages = [];
                        if(page%5==0 && page > 0) {
                            page = page-1;
                        }
                        for(var i = (page-(page%5)+1); i <= maxPage && i <= (page-(page%5)+5); i++) {
                            pages.push(i);
                        }
                        console.log(pages);
                        if(pages.length == 0) {
                            pages.push(1);
                        }
                        return res.json({
                            'data' : rows,
                            'pages' : pages
                        })
                    }
                })
            }
        })
    })

    app.get('/api/reports-ibx-download', (req, res) => {
        
        var sql     = 'SELECT'
        sql         += '    li.serial_no,'
        sql         += '    i.barcode,'
        sql         += '    i.sku_name,'
        sql         += '    c.nama AS color,'
        sql         += '    s.size,'
        sql         += '    s.cm,'
        sql         += '    s.uk,'
        sql         += '    s.us,'
        sql         += '    gdi.serial_no as outerbox,'
        sql         += '    if(isnull(gdi.is_close), "", if(gdi.is_close = 1, "CLOSE", "OPEN")) as outerbox_status,'
        sql         += '    gdi.grade as grade,'
        sql         += '    st.nama as site,'
        sql         += '    l.nama as location,'
        sql         += '    ii.type,'
        sql         += '    ii.created_at as last_trans_date'
        sql         += ' FROM labels_inner li'
        sql         += ' JOIN items i ON li.item = i.id'
        sql         += ' JOIN size s ON i.size = s.size and s.manufaktur_id = i.manufacturer'
        sql         += ' JOIN color c ON i.color = c.id'
        sql         += ' LEFT JOIN vw_grades_detail_innerbox gdi on gdi.innerbox = li.id'
        sql         += ' LEFT JOIN vw_innerbox_info ii on ii.innerbox = li.id'
        sql         += ' LEFT JOIN location l on l.id = gdi.lokasi'
        sql         += ' LEFT JOIN site st on l.site = st.id'

        connection.query(sql, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    var columns = [
                        "Serial No",
                        "Barcode",
                        "SKU Name",
                        "Color",
                        "Size",
                        "CM",
                        "UK",
                        "US",
                        "Outerbox",
                        "Outerbox Status",
                        "Grade",
                        "Site",
                        "Location",
                        "Last Trans",
                        "Trans Date",
                        "Verified Date",
                        "Reference"
                    ];
                    var fields = [
                        {
                            label:columns[0],
                            value:(row, field)=> row.serial_no
                        },
                        {
                            label:columns[1],
                            value:(row, field)=> row.barcode
                        },
                        {
                            label:columns[2],
                            value:(row, field)=> row.sku_name
                        },
                        {
                            label:columns[3],
                            value:(row, field)=> row.color
                        },
                        {
                            label:columns[4],
                            value:(row, field)=> row.size
                        },
                        {
                            label:columns[5],
                            value:(row, field)=> row.cm
                        },
                        {
                            label:columns[6],
                            value:(row, field)=> row.uk
                        },
                        {
                            label:columns[7],
                            value:(row, field)=> row.us
                        },
                        {
                            label:columns[8],
                            value:(row, field)=> row.outerbox
                        },
                        {
                            label:columns[9],
                            value:(row, field)=> row.outerbox_status
                        },
                        {
                            label:columns[10],
                            value:(row, field)=> row.grade
                        },
                        {
                            label:columns[11],
                            value:(row, field)=> row.site
                        },
                        {
                            label:columns[12],
                            value:(row, field)=> row.location
                        },
                        {
                            label:columns[13],
                            value:(row, field)=> row.last_trans
                        },
                        {
                            label:columns[14],
                            value:(row, field)=> row.last_trans_date == null ? "" : row.last_trans_date.toString().replace("T", " ").replace(".000Z", "")
                        },
                        {
                            label:columns[15],
                            value:(row, field)=> ""
                        },
                        {
                            label:columns[16],
                            value:(row, field)=> ""
                        },

                    ]
                    console.log(rows);
                    console.log(fields);
                    csv = json2csv.parse(JSON.parse(JSON.stringify(rows)), {fields: fields});
                    res.attachment("data-reports.csv");
                    return res.status(200).send(csv);
                }
            }
        })
    });
}