const { exit } = require('process');

module.exports = function(app,connection){

    app.get('/api/receiving-labels-outer-by-code', (req, res) => {
        var sql = 'SELECT ';
        sql     += '     log.outerbox, ';
        sql     += '     s.id as shipping, ';
        sql     += '     log.serial_no, ';
        sql     += '     IF ( ga.id IS NULL, go.grade, 0 ) AS grade, ';
        sql     += '     IF ( ga.id IS NULL, g.nama, "Grade A" ) AS grade_name, ';
        sql     += '     s.created_at AS shipping_date, ';
        sql     += '     s.no_so, ';
        sql     += '     s.no_surat, ';
        sql     += '     s.nama_toko, ';
        sql     += '     s.alamat, ';
        sql     += '     s.id AS shipping, ';
        sql     += '     concat( l.nama, " - ", st.nama ) AS lokasi  ';
        sql     += ' FROM vw_outerbox_logs log ';
        sql     += ' JOIN shipping s ON s.id = log.reffid ';
        sql     += ' JOIN vw_grades_outerbox vgo ON vgo.outerbox = log.outerbox ';
        sql     += ' JOIN location l ON l.id = log.lokasi ';
        sql     += ' JOIN site st ON st.id = l.site ';
        sql     += ' LEFT JOIN grades_a ga ON ga.id = vgo.grade_id ';
        sql     += ' LEFT JOIN grades_other go ON go.id = vgo.grade_id ';
        sql     += ' LEFT JOIN grades g ON g.id = go.grade  ';
        sql     += ' WHERE log.serial_no = "' + req.query.code + '"';
        sql     += ' AND log.type = "SHIPPING-VERIFIED" ';
        
        connection.query(sql, 
            function (err, rows, fields) {
                if (err)  {
                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                }
                else {
                    if(rows.length == 0) {
                        return res.json({status:false, error:'Data tidak ditemukan'});
                    }else {
                        return res.json({status:true, data:rows[0]});
                    }
                }
        })
    })

    app.get('/api/receiving-labels-inner-by-code', (req, res) => {
        var sql = 'SELECT  ';
        sql     += ' log.innerbox, ';
        sql     += '   log.serial_no, ';
        sql     += '   ifnull( g.id, 0 ) AS grade, ';
        sql     += '   IF ( g.id IS NOT NULL, g.nama, "Grade A" ) AS grade_name, ';
        sql     += '   s.created_at AS shipping_date, ';
        sql     += '   s.no_so, ';
        sql     += '   s.id AS shipping, ';
        sql     += '   s.nama_toko, ';
        sql     += '   concat( l.nama, " - ", st.nama ) AS lokasi  ';
        sql     += ' FROM vw_innerbox_logs log ';
        sql     += ' JOIN shipping s on s.id = log.reffid ';
        sql     += ' JOIN location l on l.id = log.lokasi ';
        sql     += ' JOIN site st on st.id = l.site ';
        sql     += ' LEFT JOIN labels_inner li on li.id = log.innerbox ';
        sql     += ' LEFT JOIN grades g ON g.id = log.grade  ';
        sql     += ' WHERE log.serial_no = "' + req.query.code + '" ';
        sql     += ' AND log.type = "SHIPPING-VERIFIED";';
        
        connection.query(sql, 
            function (err, rows, fields) {
                if (err)  {
                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                }
                else {
                    if(rows.length == 0) {
                        return res.json({status:false, error:'Data tidak ditemukan'});
                    }else {
                        return res.json({status:true, data:rows[0]});
                    }
                }
            })
    })

    app.get('/api/receiving-labels-inner-new-by-code', (req, res) => {
        var sql = 'SELECT  ';
        sql +=  '   li.id as innerbox,'
        sql +=  '   li.serial_no,  ';
        sql +=  '   ifnull(li.nobox_grade, 0) as grade,  ';
        sql +=  '   if(li.nobox_grade is not null, g.nama, "Grade A") as grade_name, ';
        sql +=  '   s.created_at as shipping_date,  ';
        sql +=  '   s.no_so,  ';
        sql +=  '   s.id as shipping, ';
        sql +=  '   s.nama_toko, ';
        sql +=  '   concat(l.nama, " - ", st.nama) as lokasi ';
        sql +=  '   FROM receiving_detail_innerbox sdi ';
        sql +=  'join shipping s on s.id = sdi.shipping ';
        sql +=  'join labels_inner li on sdi.innerbox = li.id ';
        sql +=  'join location l on l.id = s.lokasi ';
        sql +=  'join site st on st.id = l.site ';
        sql +=  'left join grades g on g.id = li.nobox_grade ';
        sql +=  'where s.status = 1 and li.serial_no = "' + req.query.code + '";';
        
        connection.query(sql, 
            function (err, rows, fields) {
                if (err)  {
                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                }
                else {
                    if(rows.length == 0) {
                        return res.json({status:false, error:'Data tidak ditemukan'});
                    }else {
                        return res.json({status:true, data:rows[0]});
                    }
                }
            })
    })

    app.get('/api/receiving-details-by-ids', (req, res) => {
        var id = "";
        if(Array.isArray(req.query.id)) {
            var ids = req.query.id;
            ids.forEach(e => {
                if(id == "") {
                    id = e;
                }else {
                    id += "," + e;
                }
            })
        }else {
            id = req.query.id;
        }

        var sql = "select a.*, lo.serial_no, IFNULL(d.nama, 'Grade A') as grade, ifnull(b1.total, c1.total) as total, i.info, i.color, e.nama as profil_karton";
        sql += " from vw_grades_outerbox a";
        sql += " left join labels_outer lo on lo.id = a.outerbox";
        sql += " left join grades_a b on concat('A-', b.id) = a.id";
        sql += " left join profil_karton e on e.id = b.profil_karton left join vw_grades_a_info i on i.id = b.id and i.tipe = e.tipe "
        sql += " left join (";
        sql += "     select grade_a, count(*) as total";
        sql += "     from grades_a_detail";
        sql += "     group by grade_a";
        sql += " ) b1 on b1.grade_a = b.id";
        sql += " left join grades_other c on concat('OTHER-', c.id) = a.id";
        sql += " left join grades d on c.grade = d.id";
        sql += " left join (";
        sql += "     select grade_other, count(*) as total";
        sql += "     from grades_other_detail";
        sql += "     group by grade_other";
        sql += " ) c1 on c1.grade_other = c.id";
        sql += " where a.outerbox in (" + id + ")";

        connection.query(sql, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    return res.json({status:true, data:rows});
                }
            }
        })
    })

    app.get('/api/receiving-detail-items-by-outerbox', (req, res) => {
        var sql = "select c.*, innerbox, b.serial_no";
        sql += " from vw_grades_detail_innerbox a";
        sql += " join labels_inner b on b.id = a.innerbox";
        sql += " join vw_items c on b.item = c.id";
        sql += " where outerbox = " + req.query.outerbox;

        connection.query(sql, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                var items = rows;
                var sql = 'SELECT a.*, l.nama as lokasi, s.nama as site from shipping a join location l on a.lokasi = l.id join site s on l.site = s.id where a.id = ' + req.query.shipping;
                connection.query(sql, function (err, rows, fields) {
                    return res.json({status:true, data: {
                        shipping: rows[0],
                        items: items
                    }});
                });
            }
        })
    })

    app.get('/api/receiving-detail-items-by-outerboxs', (req, res) => {

        var outerbox = "";
        if(Array.isArray(req.query.outerbox)) {
            var outerboxs = req.query.outerbox;
            outerboxs.forEach(e => {
                if(outerbox == "") {
                    outerbox = e;
                }else {
                    outerbox += "," + e;
                }
            })
        }else {
            outerbox = req.query.outerbox;
        }

        var sql = "select c.*, innerbox, b.serial_no";
        sql += " from vw_grades_detail_innerbox a";
        sql += " join labels_inner b on b.id = a.innerbox";
        sql += " join vw_items c on b.item = c.id";
        sql += " where outerbox in (" + req.query.outerbox + ")";

        connection.query(sql, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    return res.json({status:true, data:rows});
                }
            }
        })
    })

    app.post('/api/receiving-detail-save', (req, res) => {

        console.log(req.body);
        if(req.body.receiving !== 0) {

            connection.query('update receiving set tipe = "' + req.body.tipe + '", no_so = "' + req.body.no_so + '", no_surat = "' + req.body.no_surat + '", nama_toko = "' + req.body.nama_toko + '", alamat = "' + req.body.alamat + '", keterangan = "' + req.body.keterangan + '", status = "' + req.body.status + '", created_by = ' + req.body.created_by + ' where id = ' + req.body.receiving, function (err, rows, fields) {
                if (err)  {
                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                }
                else {
                    var deleteouterboxs = "0";
                    if(req.body.deletes.length > 0) {
                        req.body.deletes.forEach(e => {
                            deleteouterboxs = deleteouterboxs + "," + e;
                        })
                    }
                    var deleteinnerboxs = "0";
                    if(req.body.deletes_inner.length > 0) {
                        req.body.deletes_inner.forEach(e => {
                            deleteinnerboxs = deleteinnerboxs + "," + e;
                        })
                    }
                    var sql = "DELETE FROM receiving_detail where receiving = " + req.body.receiving + " and outerbox in (" + deleteouterboxs + ")";
                    sql += "; DELETE FROM receiving_detail_innerbox where receiving = " + req.body.receiving + " and innerbox in (" + deleteinnerboxs + ")";

                    connection.query(sql, function (err, result) {
                        if (err)  {
                            return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                        }
                        else {
                            var sql = "INSERT INTO receiving_detail (receiving,outerbox,created_at,created_by,grade,note,shipping) values ?";
                            var values = [];    
                            req.body.details.forEach(e => {
                                values.push([req.body.receiving,e[0],req.body.created_at,req.body.created_by,e[2],e[3],e[1]]);
                            })
                            
                            var sql1 = "INSERT INTO receiving_detail_innerbox (receiving,innerbox,created_at,created_by,grade,note,shipping) values ?";
                            var values_inner = [];    
                            req.body.details_inner.forEach(e => {
                                values_inner.push([req.body.receiving,e[0],req.body.created_at,req.body.created_by,e[2],e[3],e[1]]);
                            })

                            if(values.length > 0) {
                                connection.query(sql, [values], function (err, result) {
                                    if (err)  {
                                        return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                                    }
                                    else {
                                        if(values_inner.length > 0) {
                                            connection.query(sql1, [values_inner], function (err, result) {
                                                if (err)  {
                                                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                                                }
                                                else {
                                                    return res.json({ status: true, data: result.insertId});
                                                }
                                            })
                                        }else {
                                            return res.json({ status: true, data: result.insertId});
                                        }
                                    }
                                })
                            }else {
                                if(values_inner.length > 0) {
                                    connection.query(sql1, [values_inner], function (err, result) {
                                        if (err)  {
                                            return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                                        }
                                        else {
                                            return res.json({ status: true, data: result.insertId});
                                        }
                                    })
                                }else {
                                    return res.json({ status: true, data: result.insertId});
                                }
                            }
                        }
                    })
                }
            })

        }else {
            var sql = "INSERT INTO receiving (tipe,no_so,no_surat,nama_toko,alamat,status,created_at,created_by,lokasi,keterangan) values ?";
            var values = [];        
            values.push([req.body.tipe, req.body.no_so, req.body.no_surat, req.body.nama_toko, req.body.alamat, req.body.status, req.body.created_at, req.body.created_by, req.body.lokasi, req.body.keterangan]);
            connection.query(sql, [values], function (err, result) {
                if (err)  {
                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                }
                else {
                    var receiving = result.insertId;
                    var sql = "INSERT INTO receiving_detail (receiving,outerbox,created_at,created_by,grade,note,shipping) values ?";
                    var values = [];    
                    req.body.details.forEach(e => {
                        values.push([receiving,e[0],req.body.created_at,req.body.created_by,e[2],e[3],e[1]]);
                    })
                    
                    var sql1 = "INSERT INTO receiving_detail_innerbox (receiving,innerbox,created_at,created_by,grade,note,shipping) values ?";
                    var values_inner = [];    
                    req.body.details_inner.forEach(e => {
                        values_inner.push([receiving,e[0],req.body.created_at,req.body.created_by,e[2],e[3],e[1]]);
                    })

                    if(values.length > 0) {
                        connection.query(sql, [values], function (err, result) {
                            if (err)  {
                                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                            }
                            else {
                                if(values_inner.length > 0) {
                                    connection.query(sql1, [values_inner], function (err, result) {
                                        if (err)  {
                                            return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                                        }
                                        else {
                                            return res.json({ status: true, data: result.insertId});
                                        }
                                    })
                                }else {
                                    return res.json({ status: true, data: result.insertId});
                                }
                            }
                        })
                    }else {
                        if(values_inner.length > 0) {
                            connection.query(sql1, [values_inner], function (err, result) {
                                if (err)  {
                                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                                }
                                else {
                                    return res.json({ status: true, data: result.insertId});
                                }
                            })
                        }else {
                            return res.json({ status: true, data: result.insertId});
                        }
                    }
                }
            })
        }
    })

    app.get('/api/receivings', (req, res) => {
        var size = 10;
        if(req.query.size) {
            size = parseInt(req.query.size);
        }
        var limit = 0;
        var page = 1;
        var search = "";
        if(req.query.search) {
            search = req.query.search;
        }
        var lokasi = 0
        if(req.query.lokasi) {
            lokasi = req.query.lokasi;
        }
        if(req.query.page && req.query.page != "null") {
            page = parseInt(req.query.page);
            limit = (page - 1) * size;
        }
        var where = '(no_surat like "%' + search + '%" or nama_toko like "%' + search + '%" or alamat like "%' + search + '%" or no_so like "%' + search + '%") and a.lokasi = ' + lokasi;
        if(req.query.status) {
            where = where + ' and status = ' + req.query.status
        }
        if(req.query.receiving_type != undefined && req.query.receiving_type != 0) {
            where = where + ' and tipe = ' + req.query.receiving_type
        }
        if(req.query.date1 && req.query.date2) {
            where = where + " and (a.created_at between '" + req.query.date1 + " 00:00:00' and '" + req.query.date2 + " 23:59:59')";
        }

        var sql = 'SELECT ifnull(b.total,0) + ifnull(c.total,0) as total, a.*, d.nama as receiving_type, ifnull(sia.active, 1) * ifnull(soa.active, 1) as receiving_active';
        sql    += ' from receiving a';
        sql    += ' left join vw_receiving_items_total b on b.receiving = a.id';
        sql    += ' left join vw_receiving_items1_total c on c.receiving = a.id';
        sql    += ' left join vw_receiving_innerbox_active sia on sia.receiving = a.id';
        sql    += ' left join vw_receiving_outerbox_active soa on soa.receiving = a.id';
        sql    += ' join receiving_type d on d.id = a.tipe';
        sql    += ' where ' + where;
        sql    += ' order by a.created_at desc limit ' + limit + ',' + size;

        connection.query(sql, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                connection.query('SELECT count(*) as total from receiving a where ' + where, function (err1, rows1, fields1) {
                    if (err1)  {
                        return res.json({status:false, error:'Terjadi kesalahan. Err :' + JSON.stringify(err1)});
                    }
                    else {
                        var total = rows1[0].total;
                        var maxPage = Math.ceil( total / size )
                        var pages = [];
                        if(page%5==0 && page > 0) {
                            page = page-1;
                        }
                        for(var i = (page-(page%5)+1); i <= maxPage && i <= (page-(page%5)+5); i++) {
                            pages.push(i);
                        }
                        if(pages.length == 0) {
                            pages.push(1);
                        }
                        return res.json({
                            'data' : rows,
                            'pages' : pages
                        })
                    }
                })
            }
        })
    })

    app.get('/api/receiving-by-id', (req, res) => {
        var id = req.query.id;
        var sql = 'SELECT a.*, b.nama as receiving_type, u.username from receiving a join receiving_type b on a.tipe = b.id left join user u on a.created_by = u.id where a.id = ' + id;
        connection.query(sql, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    var receiving = rows[0];
                    var sql = 'SELECT a.*, ifnull(g.nama, "Grade A") as grade_name from receiving_detail a left join grades g on a.grade = g.id where a.receiving = ' + id;
                    connection.query(sql, function (err, rows, fields) {
                        if (err)  {
                            return res.json({status:false, error:'Data tidak ditemukan'});
                        }
                        else {
                            var receiving_data = {receiving : receiving, details : [], details_inner : []};
                            rows.forEach((e) => {
                                receiving_data.details.push([e.outerbox, e.shipping, e.grade, e.note, e.grade_name]);
                            })
                            var sql = 'SELECT a.*, ifnull(g.nama, "Grade A") as grade_name from receiving_detail_innerbox a left join grades g on a.grade = g.id where a.receiving = ' + id;
                            connection.query(sql, function (err, rows, fields) {
                                if (err)  {
                                    return res.json({status:false, error:'Data tidak ditemukan'});
                                }
                                else {
                                    rows.forEach((e) => {
                                        receiving_data.details_inner.push([e.innerbox, e.shipping, e.grade, e.note, e.grade_name]);
                                    })
                                    return res.json({status:true, data:receiving_data});
                                }
                            })
                        }
                    })
                }
            }
        })
    })

    app.post('/api/receiving-delete', (req, res) => {
        
        var sql = "DELETE FROM receiving where id = " + req.body.id;
        connection.query(sql, function (err, result) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                var sql = "DELETE FROM receiving where id = " + req.body.id;
                
                console.log(result);
                return res.json({ status: true, data: result.insertId});
            }
        })
    })

    app.post('/api/receiving-innerbox-out', (req, res) => {
        console.log(req.body);
        var id = req.body.id;
        var innerbox = req.body.innerbox;
        var ids = id.split("-");
        var sql = "";
        if(ids[0] === 'A') {
            sql = "UPDATE grades_a_detail set status = 0, note = '" + req.body.note + "' where grade_a = + " + ids[1] + " and innerbox = " + innerbox;
        }else {
            sql = "UPDATE grades_other_detail set status = 0, note = '" + req.body.note + "' where grade_other = + " + ids[1] + " and innerbox = " + innerbox;
        }
        connection.query(sql, function (err, result) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                return res.json({ status: true, data: result.insertId});
            }
        })
    })

    app.post("/api/receiving-uploadcsv", (req, res) => {
        var multer = require('multer');
        var path  = require('path');
        var csv = require('csv-parser');
        var fs = require('fs');

        const storage = multer.diskStorage({
            destination : path.join('uploads/csv/'),
            filename: function(req, file, cb){
                cb(null, file.fieldname + '-' + Date.now() +
                path.extname(file.originalname));
            }
        });
    
        //init upload
        const upload = multer({
            storage : storage
        }).single('csv');
    
        upload(req, res, err => {
            if (err) return res.json({ status: false, error: "Terjadi kesalahan. Err " + JSON.stringify(err)});
            const results = [];
            fs.createReadStream(path.join('uploads/csv/' + req.file.filename) )
            .pipe(csv())
            .on('data', (row) => {
                results.push(row)
            })
            .on('end', () => {
                var values = [];
                var error = "";
                results.forEach((e) => {
                    var no_so = e['No. Pesanan'];
                    if(values.indexOf(no_so)>=0) {
                        console.log(values);
                        error = "Terjadi kesalahan. Terdapat duplikasi data No Pesanan : " + no_so;
                        return;
                    }else {
                        values.push(no_so);
                    }
                });

                if(error != "") {
                    return res.json({ status: false, error: error});
                }
                return res.json({ status: true, data:req.file.filename});
            });
        });
    });

    app.post("/api/receiving-upload", (req, res) => {
        const csv = require('csv-parser');
        const fs = require('fs');
        var path  = require('path');
        const results = [];
    
        // console.log(req);
    
        fs.createReadStream(path.join('uploads/csv/' + req.body.csv) )
        .pipe(csv())
        .on('data', (row) => {
            results.push(row)
        })
        .on('end', () => {
            
            var values = [];
            results.forEach((e) => {
                values.push([
                    e['Tipe Penerimaan'],
                    e['No. Surat'],
                    e['No. Pesanan'],
                    e['Tujuan'],
                    e['Alamat'],
                    e['Keterangan'],
                    req.body.lokasi,
                    req.body.created_by
                ]);
            });

            var tmp = Math.random().toString(36).slice(2);
            var sql = "";
            values.forEach((e) => {
                sql += "select insert_receiving('" + e[0] + "','" + e[1] + "','" + e[2] + "','" + e[3] + "','" + e[4] + "','" + e[5] + "','" + e[6] + "'," + e[7] + ") as result;";
            });

            connection.query(sql, function (err, result) {
                if (err)  {
                    console.log(err);
                    return res.json({status:false, error:'Terjadi kesalahan. Data tidak bisa tersimpan.'});
                }
                else {
                    var msg = "";
                    var i = 0;
                    var s = 0;
                    result.forEach((e) => {
                        if(e[0].result == '') {
                            s++;
                        } else {
                            msg += values[i][1] + " gagal disimpan. " + e[0].result + "\n";
                        }
                        i++;
                    })
                    if(msg == '') {
                        return res.json({status:true, data:results.length});
                    } else {
                        msg = s + " data berhasil disimpan.\n\n" + msg;
                        return res.json({status:false, error:msg});
                    }
                }
            });
        });
    });
    
}