module.exports = function(app,connection){

    app.get('/api/grades-other', (req, res) => {
        var size = 10;
        if(req.query.size) {
            size = parseInt(req.query.size);
        }
        var limit = 0;
        var page = 1;
        var search = "";
        var lokasi = 0
        if(req.query.lokasi) {
            lokasi = req.query.lokasi;
        }
        if(req.query.search) {
            search = req.query.search;
        }
        if(req.query.page && req.query.page != "null") {
            page = parseInt(req.query.page);
            limit = (page - 1) * size;
        }

        var srcDate = "";
        if(req.query.date1 && req.query.date2) {
            srcDate = " and (a.created_at between '" + req.query.date1 + " 00:00:00' and '" + req.query.date2 + " 23:59:59')";
        }
        
        var sql  = 'SELECT b.*, a.*, lo.serial_no, log.type as last_status '
        sql     += ' from grades_other a '
        sql     += ' join grades b on b.id = a.grade '
        sql     += ' join labels_outer lo on lo.id = a.outerbox '
        sql     += ' join vw_outerbox_logs log on log.outerbox = a.outerbox '
        sql     += ' where (nama like "%' + search + '%" or lo.serial_no like "%' + search + '%") and log.lokasi = ' + lokasi + srcDate
        sql     += ' order by a.created_at desc limit ' + limit + ',' + size

        connection.query(sql, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                connection.query('SELECT count(*) as total from grades_other a join grades b on b.id = a.grade join labels_outer lo on lo.id = a.outerbox join vw_outerbox_logs log on log.outerbox = a.outerbox where (nama like "%' + search + '%") and log.lokasi = ' + lokasi + srcDate + '', function (err1, rows1, fields1) {
                    if (err1)  {
                        return res.json({status:false, error:'Terjadi kesalahan. Err :' + JSON.stringify(err1)});
                    }
                    else {
                        var total = rows1[0].total;
                        var maxPage = Math.ceil( total / size )
                        var pages = [];
                        if(page%5==0 && page > 0) {
                            page = page-1;
                        }
                        for(var i = (page-(page%5)+1); i <= maxPage && i <= (page-(page%5)+5); i++) {
                            pages.push(i);
                        }
                        if(pages.length == 0) {
                            pages.push(1);
                        }
                        return res.json({
                            'data' : rows,
                            'pages' : pages
                        })
                    }
                })
            }
        })
    })
    
    app.get('/api/grades-other-by-id', (req, res) => {
        var id = req.query.id;
        var sql = 'SELECT a.*, b.nama as grade_name, c.serial_no from grades_other a join grades b on a.grade = b.id join labels_outer c on a.outerbox = c.id where a.id = ' + id;
        connection.query(sql, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    var grade = rows[0];
                    var sql     = 'SELECT c.*, b.id as innerbox, b.serial_no, status, note, if(isnull(log.id), 0, 1) as shipping_status '
                    sql         += 'from grades_other_detail a '
                    sql         += 'join labels_inner b on a.innerbox = b.id '
                    sql         += 'join vw_items c on b.item = c.id '
                    sql         += 'left join labels_inner_log log on log.innerbox = b.id and log.type = "SHIPPING-NOBOX" '
                    sql         += 'where a.grade_other = ' + id;
                    
                    connection.query(sql, function (err, rows, fields) {
                        if (err)  {
                            return res.json({status:false, error:'Data tidak ditemukan'});
                        }
                        else {
                            var grade_data = {grade : grade, details : rows};
                            return res.json({status:true, data:grade_data});
                        }
                    })
                }
            }
        })
    })
    
    app.post('/api/grades-other-detail-save', (req, res) => {
        if(req.body.grade_other !== 0) {
            connection.query('update grades_other set grade = ' + req.body.grade + ', outerbox = ' + req.body.outerbox + ', is_close = ' + req.body.is_close + ', created_by = ' + req.body.created_by + ' where id = ' + req.body.grade_other, function (err, rows, fields) {
                if (err)  {
                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                }
                else {
                    var deleteinnnerboxs = "0";
                    if(req.body.deletes.length > 0) {
                        req.body.deletes.forEach(e => {
                            deleteinnnerboxs = deleteinnnerboxs + "," + e;
                        })
                    }
                    var sql = "DELETE FROM grades_other_detail where grade_other = " + req.body.grade_other + " and innerbox in (" + deleteinnnerboxs + ")";
                    connection.query(sql, function (err, result) {
                        if (err)  {
                            return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                        }
                        else {
                            if(req.body.details.length === 0) {
                                return res.json({ status: true, data: result.insertId});
                            }
                            var sql = "INSERT INTO grades_other_detail (grade_other,innerbox,created_at,created_by) values ?";
                            var values = [];    
                            req.body.details.forEach(e => {
                                values.push([req.body.grade_other,e,req.body.created_at,req.body.created_by]);
                            })    
                            connection.query(sql, [values], function (err, result) {
                                if (err)  {
                                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                                }
                                else {
                                    console.log(result);
                                    return res.json({ status: true, data: result.insertId});
                                }
                            })
                        }
                    })
                }
            })
        }else {
            var sql = "INSERT INTO grades_other (grade,outerbox,created_at,created_by,close_at,lokasi,is_close) values ?";
            var values = [];        
            values.push([req.body.grade, req.body.outerbox, req.body.created_at, req.body.created_by, req.body.close_at, req.body.lokasi,req.body.is_close]);
            connection.query(sql, [values], function (err, result) {
                if (err)  {
                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                }
                else {
                    var grade_other = result.insertId;
                    var sql = "INSERT INTO grades_other_detail (grade_other,innerbox,created_at,created_by) values ?";
                    var values = [];    
                    req.body.details.forEach(e => {
                        values.push([grade_other,e,req.body.created_at,req.body.created_by]);
                    })    
                    connection.query(sql, [values], function (err, result) {
                        if (err)  {
                            return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                        }
                        else {
                            console.log(result);
                            return res.json({ status: true, data: result.insertId});
                        }
                    })
                }
            })
        }
    })
    
    app.post('/api/grades-other-delete', (req, res) => {
        
        var sql = "DELETE FROM grades_other where id = " + req.body.id;
        connection.query(sql, function (err, result) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                console.log(result);
                return res.json({ status: true, data: result.insertId});
            }
        })
    })
}