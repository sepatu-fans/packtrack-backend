module.exports = function(app,connection){

    app.get('/api/grades', (req, res) => {
        var size = 10;
        if(req.query.size) {
            size = parseInt(req.query.size);
        }
        var limit = 0;
        var page = 1;
        var search = "";
        if(req.query.search) {
            search = req.query.search;
        }
        if(req.query.page && req.query.page != "null") {
            page = parseInt(req.query.page);
            limit = (page - 1) * size;
        }
        connection.query('SELECT a.* from grades a where (a.nama like "%' + search + '%") order by a.nama limit ' + limit + ',' + size, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                connection.query('SELECT count(*) as total from grades a where (a.nama like "%' + search + '%")', function (err1, rows1, fields1) {
                    if (err1)  {
                        return res.json({status:false, error:'Terjadi kesalahan. Err :' + JSON.stringify(err1)});
                    }
                    else {
                        var total = rows1[0].total;
                        var maxPage = Math.ceil( total / size )
                        var pages = [];
                        if(page%5==0 && page > 0) {
                            page = page-1;
                        }
                        for(var i = (page-(page%5)+1); i <= maxPage && i <= (page-(page%5)+5); i++) {
                            pages.push(i);
                        }
                        if(pages.length == 0) {
                            pages.push(1);
                        }
                        return res.json({
                            'data' : rows,
                            'pages' : pages
                        })
                    }
                })
            }
        })
    })
    
    app.get('/api/grades-list', (req, res) => {
        connection.query('SELECT * from grades', function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                return res.json({status:true, data:rows});
            }
        })
    })

    app.post('/api/grades-add', (req, res) => {
        
        if(req.body.id === undefined) {
            var sql = "INSERT INTO grades (nama,created_at,created_by) values ?";
            var values = [];
            values.push([req.body.nama, req.body.created_at, req.body.created_by]);
            connection.query(sql, [values], function (err, result) {
                if (err)  {
                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                }
                else {
                    console.log(result);
                    return res.json({ status: true, data: result.insertId});
                }
            })
        }else {
            connection.query("update grades set nama = '" + req.body.nama + "' where id =" + req.body.id, function (err, result) {
                if (err)  {
                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                }
                else {
                    console.log(result);
                    return res.json({ status: true, data: result.insertId});
                }
            })
        }
    })

    app.post('/api/grades-delete', (req, res) => {
        
        var sql = "DELETE FROM grades where id = " + req.body.id;
        connection.query(sql, function (err, result) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                console.log(result);
                return res.json({ status: true, data: result.insertId});
            }
        })
    })

    app.get('/api/grades-outerbox-check', (req, res) => {
        var sql = 'SELECT * '
        sql     += 'from vw_outerbox_logs logs '
        sql     += 'where logs.type = "AVAILABLE" and logs.outerbox = ' + req.query.outerbox;

        connection.query(sql, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return  res.json({status:false, error:'Label Outerbox tidak tersedia'});
                }else {
                    return  res.json({status:true});
                }
            }
        })
    })
    
    app.get('/api/grades-by-id', (req, res) => {
        var id = req.query.id;
        connection.query('SELECT a.* from grades a where id = ' + id + '', function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    return res.json({status:true, data:rows[0]});
                }
            }
        })
    })
}
