const e = require('express')

module.exports = function(app,connection){

    app.get('/api/shipping-labels-outer-by-code', (req, res) => {
        var sql = 'SELECT logs.*, ifnull(g.nama, "Grade A") as grade_name, ifnull(ga.is_close, go.is_close) as is_close '
        sql     += 'from vw_outerbox_logs logs '
        sql     += 'join labels_outer lo on lo.id = logs.outerbox '
        sql     += 'left join grades_a ga on ga.id = logs.reffid '
        sql     += 'left join grades_other go on go.id = logs.reffid '
        sql     += 'left join grades g on logs.grade = g.id '
        sql     += 'where (logs.type in ("GRADE-A","GRADE-OTHER","RECEIVING-VERIFIED","TRANSFER-VERIFIED") or (logs.type = "SHIPPING" and logs.reffid = ' + req.query.shipping + ')) and lo.serial_no = "' + req.query.code + '"';
        
        connection.query(sql, 
            function (err, rows, fields) {
                if (err)  {
                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                }
                else {
                    if(rows.length == 0) {
                        return res.json({status:false, error:'Label tidak tersedia'});
                    }else {
                        return res.json({status:true, data:rows[0]});
                    }
                }
            })
    })

    app.get('/api/shipping-details-by-ids', (req, res) => {
        var id = "";
        if(Array.isArray(req.query.id)) {
            var ids = req.query.id;
            ids.forEach(e => {
                if(id == "") {
                    id = e;
                }else {
                    id += "," + e;
                }
            })
        }else {
            id = req.query.id;
        }

        var sql = "select a.*, lo.serial_no, IFNULL(d.nama, 'Grade A') as grade, ifnull(b1.total, c1.total) as total, i.info, i.color, e.nama as profil_karton";
        sql += " from vw_grades_outerbox a";
        sql += " left join labels_outer lo on lo.id = a.outerbox";
        sql += " left join grades_a b on concat('A-', b.id) = a.id";
        sql += " left join profil_karton e on e.id = b.profil_karton left join vw_grades_a_info i on i.id = b.id and i.tipe = e.tipe "
        sql += " left join (";
        sql += "     select grade_a, count(*) as total";
        sql += "     from grades_a_detail";
        sql += "     group by grade_a";
        sql += " ) b1 on b1.grade_a = b.id";
        sql += " left join grades_other c on concat('OTHER-', c.id) = a.id";
        sql += " left join grades d on c.grade = d.id";
        sql += " left join (";
        sql += "     select grade_other, count(*) as total";
        sql += "     from grades_other_detail";
        sql += "     group by grade_other";
        sql += " ) c1 on c1.grade_other = c.id";
        sql += " where a.outerbox in (" + id + ")";

        connection.query(sql, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    return res.json({status:true, data:rows});
                }
            }
        })
    })

    app.get('/api/shipping-detail-items-by-outerbox', (req, res) => {
        var sql = "select c.*, innerbox, b.serial_no";
        sql += " from vw_grades_detail_innerbox a";
        sql += " join labels_inner b on b.id = a.innerbox";
        sql += " join vw_items c on b.item = c.id";
        sql += " where outerbox = " + req.query.outerbox;

        connection.query(sql, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    return res.json({status:true, data:rows});
                }
            }
        })
    })

    app.get('/api/shipping-detail-items-by-outerboxs', (req, res) => {

        var outerbox = "";
        if(Array.isArray(req.query.outerbox)) {
            var outerboxs = req.query.outerbox;
            outerboxs.forEach(e => {
                if(outerbox == "") {
                    outerbox = e;
                }else {
                    outerbox += "," + e;
                }
            })
        }else {
            outerbox = req.query.outerbox;
        }

        var sql = "select c.*, innerbox, b.serial_no";
        sql += " from vw_grades_detail_innerbox a";
        sql += " join labels_inner b on b.id = a.innerbox";
        sql += " join vw_items c on b.item = c.id";
        sql += " where outerbox in (" + req.query.outerbox + ")";

        connection.query(sql, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    return res.json({status:true, data:rows});
                }
            }
        })
    })

    app.post('/api/shipping-detail-save', (req, res) => {
        if(req.body.shipping !== 0) {
            connection.query('update shipping set tipe = "' + req.body.tipe + '", no_so = "' + req.body.no_so + '", no_surat = "' + req.body.no_surat + '", nama_toko = "' + req.body.nama_toko + '", alamat = "' + req.body.alamat + '", keterangan = "' + req.body.keterangan + '", status = "' + req.body.status + '", created_by = ' + req.body.created_by + ' where id = ' + req.body.shipping, function (err, rows, fields) {
                if (err)  {
                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                }
                else {
                    var deleteouterboxs = "0";
                    if(req.body.deletes.length > 0) {
                        req.body.deletes.forEach(e => {
                            deleteouterboxs = deleteouterboxs + "," + e[0];
                        })
                    }
                    var deleteinnerboxs = "0";
                    if(req.body.deletes_inner.length > 0) {
                        req.body.deletes_inner.forEach(e => {
                            deleteinnerboxs = deleteinnerboxs + "," + e[0];
                        })
                    }
                    var sql = "DELETE FROM shipping_detail where shipping = " + req.body.shipping + " and outerbox in (" + deleteouterboxs + ")";
                    sql += "; DELETE FROM shipping_detail_innerbox where shipping = " + req.body.shipping + " and innerbox in (" + deleteinnerboxs + ")";

                    connection.query(sql, function (err, result) {
                        if (err)  {
                            return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                        }
                        else {
                            var sql = "INSERT INTO shipping_detail (shipping,outerbox,created_at,created_by,grade) values ?";
                            var values = [];    
                            req.body.details.forEach(e => {
                                values.push([req.body.shipping,e[0],req.body.created_at,req.body.created_by,e[1]]);
                            })
                            
                            var sql1 = "INSERT INTO shipping_detail_innerbox (shipping,innerbox,created_at,created_by,grade) values ?";
                            var values_inner = [];    
                            req.body.details_inner.forEach(e => {
                                values_inner.push([req.body.shipping,e[0],req.body.created_at,req.body.created_by,e[1]]);
                            })

                            if(values.length > 0) {
                                connection.query(sql, [values], function (err, result) {
                                    if (err)  {
                                        return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                                    }
                                    else {
                                        if(values_inner.length > 0) {
                                            connection.query(sql1, [values_inner], function (err, result) {
                                                if (err)  {
                                                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                                                }
                                                else {
                                                    return res.json({ status: true, data: result.insertId});
                                                }
                                            })
                                        }else {
                                            return res.json({ status: true, data: result.insertId});
                                        }
                                    }
                                })
                            }else {
                                if(values_inner.length > 0) {
                                    connection.query(sql1, [values_inner], function (err, result) {
                                        if (err)  {
                                            return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                                        }
                                        else {
                                            return res.json({ status: true, data: result.insertId});
                                        }
                                    })
                                }else {
                                    return res.json({ status: true, data: result.insertId});
                                }
                            }
                        }
                    })
                }
            })
        }else {
            var sql = "INSERT INTO shipping (tipe,no_so,no_surat,nama_toko,alamat,status,created_at,created_by,lokasi,keterangan) values ?";
            var values = [];        
            values.push([req.body.tipe, req.body.no_so, req.body.no_surat, req.body.nama_toko, req.body.alamat, req.body.status, req.body.created_at, req.body.created_by, req.body.lokasi, req.body.keterangan]);
            connection.query(sql, [values], function (err, result) {
                if (err)  {
                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                }
                else {
                    var shipping = result.insertId;
                    var sql = "INSERT INTO shipping_detail (shipping,outerbox,created_at,created_by,grade) values ?";
                    var values = [];    
                    req.body.details.forEach(e => {
                        values.push([shipping,e[0],req.body.created_at,req.body.created_by,e[1]]);
                    })
                    
                    var sql1 = "INSERT INTO shipping_detail_innerbox (shipping,innerbox,created_at,created_by,grade) values ?";
                    var values_inner = [];    
                    req.body.details_inner.forEach(e => {
                        values_inner.push([shipping,e[0],req.body.created_at,req.body.created_by,e[1]]);
                    })

                    if(values.length > 0) {
                        connection.query(sql, [values], function (err, result) {
                            if (err)  {
                                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                            }
                            else {
                                if(values_inner.length > 0) {
                                    connection.query(sql1, [values_inner], function (err, result) {
                                        if (err)  {
                                            return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                                        }
                                        else {
                                            return res.json({ status: true, data: result.insertId});
                                        }
                                    })
                                }else {
                                    return res.json({ status: true, data: result.insertId});
                                }
                            }
                        })
                    }else {
                        if(values_inner.length > 0) {
                            connection.query(sql1, [values_inner], function (err, result) {
                                if (err)  {
                                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                                }
                                else {
                                    return res.json({ status: true, data: result.insertId});
                                }
                            })
                        }else {
                            return res.json({ status: true, data: result.insertId});
                        }
                    }
                }
            })
        }
    })

    app.get('/api/shippings', (req, res) => {
        var size = 10;
        if(req.query.size) {
            size = parseInt(req.query.size);
        }
        var limit = 0;
        var page = 1;
        var search = "";
        if(req.query.search) {
            search = req.query.search;
        }
        var lokasi = 0
        if(req.query.lokasi) {
            lokasi = req.query.lokasi;
        }
        if(req.query.page && req.query.page != "null") {
            page = parseInt(req.query.page);
            limit = (page - 1) * size;
        }
        var where = '(no_surat like "%' + search + '%" or nama_toko like "%' + search + '%" or alamat like "%' + search + '%" or no_so like "%' + search + '%") and a.lokasi = ' + lokasi;
        if(req.query.status) {
            where = where + ' and status = ' + req.query.status
        }
        if(req.query.shipping_type != undefined && req.query.shipping_type != 0) {
            where = where + ' and tipe = ' + req.query.shipping_type
        }
        if(req.query.date1 && req.query.date2) {
            where = where + " and (a.created_at between '" + req.query.date1 + " 00:00:00' and '" + req.query.date2 + " 23:59:59')";
        }
        console.log(where);
        var sql = 'SELECT ifnull(b.total,0) + ifnull(c.total,0) as total, a.*, d.nama as shipping_type, ifnull(sia.active, 1) * ifnull(soa.active, 1) as shipping_active';
        sql    += ' from shipping a';
        sql    += ' left join vw_shipping_items_total b on b.shipping = a.id';
        sql    += ' left join vw_shipping_items1_total c on c.shipping = a.id';
        sql    += ' left join vw_shipping_innerbox_active sia on sia.shipping = a.id';
        sql    += ' left join vw_shipping_outerbox_active soa on soa.shipping = a.id';
        sql    += ' join shipping_type d on d.id = a.tipe';
        sql    += ' where ' + where;
        sql    += ' order by a.created_at desc limit ' + limit + ',' + size;

        connection.query(sql, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                connection.query('SELECT count(*) as total from shipping a where ' + where + '', function (err1, rows1, fields1) {
                    if (err1)  {
                        return res.json({status:false, error:'Terjadi kesalahan. Err :' + JSON.stringify(err1)});
                    }
                    else {
                        var total = rows1[0].total;
                        var maxPage = Math.ceil( total / size )
                        var pages = [];
                        if(page%5==0 && page > 0) {
                            page = page-1;
                        }
                        for(var i = (page-(page%5)+1); i <= maxPage && i <= (page-(page%5)+5); i++) {
                            pages.push(i);
                        }
                        if(pages.length == 0) {
                            pages.push(1);
                        }
                        return res.json({
                            'data' : rows,
                            'pages' : pages
                        })
                    }
                })
            }
        })
    })

    app.get('/api/shipping-by-id', (req, res) => {
        var id = req.query.id;
        var sql = 'SELECT a.*, b.nama as shipping_type, u.username from shipping a join shipping_type b on a.tipe = b.id left join user u on a.created_by = u.id where a.id = ' + id;
        connection.query(sql, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    var shipping = rows[0];
                    var sql = 'SELECT outerbox, ifnull(g.nama, "Grade A") as grade_name from shipping_detail a left join grades g on a.grade = g.id where a.shipping = ' + id;
                    connection.query(sql, function (err, rows, fields) {
                        if (err)  {
                            return res.json({status:false, error:'Data tidak ditemukan'});
                        }
                        else {
                            var shipping_data = {shipping : shipping, details : [], details_inner : []};
                            rows.forEach((e) => {
                                shipping_data.details.push([e.outerbox, e.grade_name]);
                            })
                            var sql = 'SELECT innerbox, g.id as grade, ifnull(g.nama, "Grade A") as grade_name from shipping_detail_innerbox a left join grades g on a.grade = g.id where a.shipping = ' + id;
                            connection.query(sql, function (err, rows, fields) {
                                if (err)  {
                                    return res.json({status:false, error:'Data tidak ditemukan'});
                                }
                                else {
                                    rows.forEach((e) => {
                                        shipping_data.details_inner.push([e.innerbox, e.grade, e.grade_name]);
                                    })
                                    return res.json({status:true, data:shipping_data});
                                }
                            })
                        }
                    })
                }
            }
        })
    })

    app.get('/api/shipping-by-ids', (req, res) => {
        var id = "";
        if(Array.isArray(req.query.id)) {
            var ids = req.query.id;
            ids.forEach(e => {
                if(id == "") {
                    id = e;
                }else {
                    id += "," + e;
                }
            })
        }else {
            id = req.query.id;
        }
        var sql = 'SELECT a.*, b.nama as shipping_type, ';
        sql +=  ' concat(l.nama, " - ", st.nama) as lokasi ';
        sql +=  ' from shipping a ';
        sql +=  ' join shipping_type b on a.tipe = b.id ';
        sql +=  ' join location l on l.id = a.lokasi ';
        sql +=  ' join site st on st.id = l.site ';
        sql +=  ' where a.id in (' + id + ')';
        connection.query(sql, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    return res.json({status:true, data:rows});
                }
            }
        })
    })

    app.post('/api/shipping-delete', (req, res) => {
        
        var sql = "DELETE FROM shipping where id = " + req.body.id;
        connection.query(sql, function (err, result) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                console.log(result);
                return res.json({ status: true, data: result.insertId});
            }
        })
    })

    app.get('/api/shipping-innerbox-check', (req, res) => {
        var sql  = 'SELECT * FROM vw_innerbox_logs';
        sql     += ' where innerbox = ' + req.query.innerbox;
        sql     += ' and (type in ("NOBOX", "RECEIVING-VERIFIED","TRANSFER-VERIFIED", "GRADE-A", "GRADE-A-OPEN", "GRADE-A-CLOSE", "GRADE-OTHER", "GRADE-OTHER-OPEN", "GRADE-OTHER-CLOSE")'
        sql     += ' or (type = "SHIPPING-NOBOX" and reffid = ' + req.query.shipping + '))';
        sql     += ' and lokasi = ' + req.query.lokasi;

        connection.query(sql, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return  res.json({status:false, error:'Label Innerbox tidak tersedia'});
                }else {
                    var innerboxdata = rows[0];
                    if(innerboxdata.type == "NOBOX") {
                        return  res.json({status:true});
                    } else if(innerboxdata.type == "SHIPPING-NOBOX") {
                        return  res.json({status:true});
                    } else if(innerboxdata.type == "RECEIVING-VERIFIED") {
                        return  res.json({status:true});
                    } else if(innerboxdata.type == "TRANSFER-VERIFIED") {
                        return  res.json({status:true});
                    } else {
                        var sql = 'SELECT gdi.*'
                        sql     += ' from vw_grades_detail_innerbox gdi '
                        sql     += ' where gdi.innerbox = ' + req.query.innerbox
                        connection.query(sql, function (err, rows, fields) {
                            if (err)  {
                                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                            }
                            else {
                                if(rows.length > 0) {
                                    if(innerboxdata.type == "GRADE-A-OPEN" || innerboxdata.type == "GRADE-OTHER-OPEN") {
                                        return  res.json({status:false,data:innerboxdata});
                                    } else {
                                        var alert = innerboxdata.serial_no + " Berada di " + rows[0].serial_no + " yang masih berstatus closed, silakan buka obx terlebih dahulu untuk dapat memasukkan barang ini ke daftar pengiriman"
                                        return  res.json({status:false, error:alert});
                                    }
                                }else {
                                    return  res.json({status:false, error:'Label Innerbox tidak tersedia'});
                                }
                            }
                        })
                    }
                }
            }
        })
    })

    app.post('/api/shipping-innerbox-out', (req, res) => {
        console.log(req.body);
        var id = req.body.id;
        var innerbox = req.body.innerbox;
        var tipe = req.body.tipe;
        var sql = "";
        if(tipe === 'GRADE-A-OPEN') {
            sql = "UPDATE grades_a_detail set status = 0, note = '" + req.body.note + "' where grade_a = + " + id + " and innerbox = " + innerbox;
        }else {
            sql = "UPDATE grades_other_detail set status = 0, note = '" + req.body.note + "' where grade_other = + " + id + " and innerbox = " + innerbox;
        }
        connection.query(sql, function (err, result) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                return res.json({ status: true, data: result.insertId});
            }
        })
    })

    app.post("/api/shipping-uploadcsv", (req, res) => {
        var multer = require('multer');
        var path  = require('path');
        var csv = require('csv-parser');
        var fs = require('fs');

        const storage = multer.diskStorage({
            destination : path.join('uploads/csv/'),
            filename: function(req, file, cb){
                cb(null, file.fieldname + '-' + Date.now() +
                path.extname(file.originalname));
            }
        });
    
        //init upload
        const upload = multer({
            storage : storage
        }).single('csv');
    
        upload(req, res, err => {
            if (err) return res.json({ status: false, error: "Terjadi kesalahan. Err " + JSON.stringify(err)});
            const results = [];
            fs.createReadStream(path.join('uploads/csv/' + req.file.filename) )
            .pipe(csv())
            .on('data', (row) => {
                results.push(row)
            })
            .on('end', () => {
                var values = [];
                var error = "";
                results.forEach((e) => {
                    var no_so = e['No. Pesanan'];
                    if(values.indexOf(no_so)>=0) {
                        error = "Terjadi kesalahan. Terdapat duplikasi data No Pesanan : " + no_so;
                        return;
                    }else {
                        values.push(no_so);
                    }
                });

                if(error != "") {
                    return res.json({ status: false, error: error});
                }
                return res.json({ status: true, data:req.file.filename});
            });
        });
    });

    app.post("/api/shipping-upload", (req, res) => {
        const csv = require('csv-parser');
        const fs = require('fs');
        var path  = require('path');
        const results = [];
    
        // console.log(req);
    
        fs.createReadStream(path.join('uploads/csv/' + req.body.csv) )
        .pipe(csv())
        .on('data', (row) => {
            results.push(row)
        })
        .on('end', () => {
            
            var values = [];
            results.forEach((e) => {
                values.push([
                    e['Tipe Pengeluaran'],
                    e['No. Surat'],
                    e['No. Pesanan'],
                    e['Tujuan'],
                    e['Alamat'],
                    e['Keterangan'],
                    req.body.lokasi,
                    req.body.created_by
                ]);
            });

            var tmp = Math.random().toString(36).slice(2);
            var sql = "";
            values.forEach((e) => {
                sql += "select insert_shipping('" + e[0] + "','" + e[1] + "','" + e[2] + "','" + e[3] + "','" + e[4] + "','" + e[5] + "','" + e[6] + "'," + e[7] + ") as result;";
            });

            connection.query(sql, function (err, result) {
                if (err)  {
                    console.log(err);
                    return res.json({status:false, error:'Terjadi kesalahan. Data tidak bisa tersimpan.'});
                }
                else {
                    var msg = "";
                    var i = 0;
                    var s = 0;
                    result.forEach((e) => {
                        if(e[0].result == '') {
                            s++;
                        } else {
                            msg += values[i][1] + " gagal disimpan. " + e[0].result + "\n";
                        }
                        i++;
                    })
                    if(msg == '') {
                        return res.json({status:true, data:results.length});
                    } else {
                        msg = s + " data berhasil disimpan.\n\n" + msg;
                        return res.json({status:false, error:msg});
                    }
                }
            });
        });
    });
    
}