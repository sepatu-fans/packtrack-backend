module.exports = function(app,connection){

    app.get('/api/user', (req, res) => {
        var size = 10;
        if(req.query.size) {
            size = parseInt(req.query.size);
        }
        var limit = 0;
        var page = 1;
        var search = "";
        if(req.query.search) {
            search = req.query.search;
        }
        if(req.query.page && req.query.page != "null") {
            page = parseInt(req.query.page);
            limit = (page - 1) * size;
        }
        connection.query('SELECT u.*, r.nama as role from user u join role r on u.role_id = r.id where (username like "%' + search + '%") order by username limit ' + limit + ',' + size, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                connection.query('SELECT count(*) as total from user where (username like "%' + search + '%")', function (err1, rows1, fields1) {
                    if (err1)  {
                        return res.json({status:false, error:'Terjadi kesalahan. Err :' + JSON.stringify(err1)});
                    }
                    else {
                        var total = rows1[0].total;
                        var maxPage = Math.ceil( total / size )
                        var pages = [];
                        if(page%5==0 && page > 0) {
                            page = page-1;
                        }
                        for(var i = (page-(page%5)+1); i <= maxPage && i <= (page-(page%5)+5); i++) {
                            pages.push(i);
                        }
                        if(pages.length == 0) {
                            pages.push(1);
                        }
                        return res.json({
                            'data' : rows,
                            'pages' : pages
                        })
                    }
                })
            }
        })
    })

    app.post('/api/user-add', (req, res) => {
        
        if(req.body.id === undefined) {
            var sql = "INSERT INTO user (username,password,role_id,created_at,created_by) values ?";
            var values = [];
            values.push([req.body.username,req.body.password,req.body.role_id,req.body.created_at,req.body.created_by]);
            connection.query(sql, [values], function (err, result) {
                if (err)  {
                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                }
                else {
                    console.log(result);
                    return res.json({ status: true, data: result.insertId});
                }
            })
        }else {
            if(req.body.password !== '') {
                connection.query("update user set username = '" + req.body.username + "', role_id = '" + req.body.role_id + "', password = '" + req.body.password + "' where id =" + req.body.id, function (err, result) {
                    if (err)  {
                        return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                    }
                    else {
                        console.log(result);
                        return res.json({ status: true, data: result.insertId});
                    }
                })
            }else {
                connection.query("update user set username = '" + req.body.username + "', role_id = '" + req.body.role_id + "' where id =" + req.body.id, function (err, result) {
                    if (err)  {
                        return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                    }
                    else {
                        console.log(result);
                        return res.json({ status: true, data: result.insertId});
                    }
                })
            }
        }
    })

    app.post('/api/user-change-password', (req, res) => {
        
        connection.query("update user set password = '" + req.body.password + "' where username ='" + req.body.username + "'", function (err, result) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                console.log(result);
                return res.json({ status: true, data: result.insertId});
            }
        })
    })

    app.post('/api/user-delete', (req, res) => {
        
        var sql = "DELETE FROM user where id = " + req.body.id;
        connection.query(sql, function (err, result) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                console.log(result);
                return res.json({ status: true, data: result.insertId});
            }
        })
    })

    app.get('/api/user-list', (req, res) => {
        connection.query('SELECT * from user order by username', function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    return res.json({status:true, data:rows});
                }
            }
        })
    })

    app.post('/api/user-login', (req, res) => {
        connection.query("SELECT u.*, r.nama as role, r.access from user u join role r on u.role_id = r.id where username = '" + req.body.username + "' and password = '" + req.body.password + "'", function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Username atau password salah'});
                }else {
                    var user = rows[0];
                    connection.query('SELECT a.*, b.nama as nama_site from location a join site b on a.site = b.id order by b.nama, a.nama', function (err, rows, fields) {
                        if (err)  {
                            return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                        }
                        else {
                            var locationAccess = [];
                            var defaultLocation = false;
                            if(user.lokasi != null && user.lokasi != "") {
                                var location = JSON.parse(user.lokasi);
                                rows.forEach(item => {
                                    location.forEach(loc => {
                                        if(item.id == loc[0] && loc[1]) {
                                            locationAccess.push(item);
                                            if(user.lokasi_terakhir != "" && user.lokasi_terakhir != "null" && user.lokasi_terakhir == item.id) {
                                                defaultLocation = item;
                                            }
                                        }
                                    })
                                });
                            }
                            if(locationAccess.length == 0) {
                                return res.json({status:false, error:'Terjadi kesalahan. User tidak punya akses ke lokasi'});
                            }else {
                                if(!defaultLocation) {
                                    defaultLocation = locationAccess[0]
                                }
                                return res.json({status:true, data:user, locations: locationAccess, defaultLocation: defaultLocation});
                            }
                        }
                    })
                }
            }
        })
    })
    
    app.get('/api/user-by-id', (req, res) => {
        var id = req.query.id;
        connection.query('SELECT a.* from user a where id = ' + id + '', function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    return res.json({status:true, data:rows[0]});
                }
            }
        })
    })

    app.post('/api/user-location', (req, res) => {
        
        connection.query("update user set lokasi = '" + req.body.access + "' where id =" + req.body.id, function (err, result) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                console.log(result);
                return res.json({ status: true, data: result.insertId});
            }
        })
    })

    app.post('/api/set-user-location', (req, res) => {
        
        connection.query("update user set lokasi_terakhir = '" + req.body.lokasi + "' where id =" + req.body.id, function (err, result) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                console.log(result);
                return res.json({ status: true, data: result.insertId});
            }
        })
    })

}