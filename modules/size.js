const json2csv = require('json2csv');
module.exports = function(app,connection){

    app.get('/api/size', (req, res) => {
        var size = 10;
        if(req.query.size) {
            size = parseInt(req.query.size);
        }
        var limit = 0;
        var page = 1;
        var search = "";
        if(req.query.search) {
            search = req.query.search;
        }
        if(req.query.page && req.query.page != "null") {
            page = parseInt(req.query.page);
            limit = (page - 1) * size;
        }
        var where = "";
        if(req.query.manufaktur) {
            where = " and manufaktur_id = " + req.query.manufaktur;
        }
        connection.query('SELECT s.*, m.nama from size s join manufaktur m on s.manufaktur_id = m.id where (nama like "%' + search + '%") ' + where + ' order by nama limit ' + limit + ',' + size, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                connection.query('SELECT count(*) as total from size s join manufaktur m on s.manufaktur_id = m.id where (nama like "%' + search + '%") ' + where, function (err1, rows1, fields1) {
                    if (err1)  {
                        return res.json({status:false, error:'Terjadi kesalahan. Err :' + JSON.stringify(err1)});
                    }
                    else {
                        var total = rows1[0].total;
                        var maxPage = Math.ceil( total / size )
                        var pages = [];
                        if(page%5==0 && page > 0) {
                            page = page-1;
                        }
                        for(var i = (page-(page%5)+1); i <= maxPage && i <= (page-(page%5)+5); i++) {
                            pages.push(i);
                        }
                        if(pages.length == 0) {
                            pages.push(1);
                        }
                        return res.json({
                            'data' : rows,
                            'pages' : pages
                        })
                    }
                })
            }
        })
    })

    app.post('/api/size-add', (req, res) => {
        
        if(req.body.id === undefined) {
            var sql = "INSERT INTO size (manufaktur_id,size,uk,us,cm,mm_p,mm_l,mm_g,created_at,created_by) values ?";
            var values = [];
            values.push([req.body.manufaktur_id,req.body.size,req.body.uk,req.body.us,req.body.cm,req.body.mm_p,req.body.mm_l,req.body.mm_g,req.body.created_at,req.body.created_by]);
            connection.query(sql, [values], function (err, result) {
                if (err)  {
                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                }
                else {
                    console.log(result);
                    return res.json({ status: true, data: result.insertId});
                }
            })
        }else {
            connection.query("update size set manufaktur_id = " + req.body.manufaktur_id + ",size='" + req.body.size + "',uk='" + req.body.uk + "',us='" + req.body.us + "',cm='" + req.body.cm + "',mm_p='" + req.body.mm_p + "',mm_l='" + req.body.mm_l + "',mm_g='" + req.body.mm_g + "' where id =" + req.body.id, function (err, result) {
                if (err)  {
                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                }
                else {
                    console.log(result);
                    return res.json({ status: true, data: result.insertId});
                }
            })
        }
    })

    app.post("/api/size-uploadcsv", (req, res) => {
        var multer = require('multer');
        var path  = require('path');
        const storage = multer.diskStorage({
            destination : 'uploads/csv/',
            filename: function(req, file, cb){
                cb(null, file.fieldname + '-' + Date.now() +
                path.extname(file.originalname));
            }
        });
    
        //init upload
        const upload = multer({
        storage : storage
        }).single('csv');
    
        upload(req, res, err => {
        if (err) return res.json({ status: false, error: "Terjadi kesalahan. Err " + err.sqlMessage});
        return res.json({ status: true, data:req.file.filename});
        });
    });

    app.post("/api/size-upload", (req, res) => {
        const csv = require('csv-parser');
        const fs = require('fs');
        var path  = require('path');
        const results = [];
    
        // console.log(req);
    
        fs.createReadStream(path.join('uploads/csv/' + req.body.csv) )
        .pipe(csv())
        .on('data', (row) => {
            results.push(row)
        })
        .on('end', () => {
            
            console.log(results);
            // var sql = "INSERT INTO items (code,barcode,name,description,sku,sku_name,sales_price,unit_code,category_code,vendor_code,vendor_item_code,vendor_item_name,manufacturer,color,size,status,discontinue) values ?";
            var values = [];
            results.forEach((e) => {
                values.push([
                    e['Manufacturer'],
                    getDouble(e['Size']),	
                    getDouble(e['UK']),	
                    getDouble(e['US']),
                    getDouble(e['CM']),	
                    getDouble(e['MM(P)']),
                    getDouble(e['MM(L)']),
                    getDouble(e['MM(G)']),
                    req.body.created_by
                ]);
            });
            console.log(values);

            var tmp = Math.random().toString(36).slice(2);
            var sql = "";
            values.forEach((e) => {
                sql += "select insert_size('" + e[0] + "'," + e[1] + "," + e[2] + "," + e[3] + "," + e[4] + "," + e[5] + "," + e[6] + "," + e[7] + ",'" + tmp + "'," + e[8] + ") as result;";
            });

            connection.query(sql, function (err, result) {
                if (err)  {
                    return res.json({status:false, error:'Terjadi kesalahan. Data tidak bisa tersimpan.' + JSON.stringify(err)});
                }
                else {
                    var msg = "";
                    var i = 0;
                    var s = 0;
                    result.forEach((e) => {
                        if(e[0].result == '') {
                            s++;
                        } else {
                            msg += values[i][1] + " gagal disimpan. " + e[0].result + "\n";
                        }
                        i++;
                    })
                    if(msg == '') {
                        return res.json({status:true, data:results.length});
                    } else {
                        msg = s + " data berhasil disimpan.\n\n" + msg;
                        return res.json({status:false, error:msg});
                    }
                }
            });
        });
    });

    function getDouble(val) {
        val = parseFloat(val);
        return isNaN(val) ? 0 : val; 
    }

    app.post('/api/size-delete', (req, res) => {
        
        var sql = "DELETE FROM size where id = " + req.body.id;
        connection.query(sql, function (err, result) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                console.log(result);
                return res.json({ status: true, data: result.insertId});
            }
        })
    })

    app.get('/api/size-list', (req, res) => {
        connection.query('SELECT * from size where manufaktur_id = ' + req.query.manufaktur, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    return res.json({status:true, data:rows});
                }
            }
        })
    })
    
    app.get('/api/size-by-id', (req, res) => {
        var id = req.query.id;
        connection.query('SELECT a.* from size a where id = ' + id + '', function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    return res.json({status:true, data:rows[0]});
                }
            }
        })
    })

    app.get('/api/size-download', (req, res) => {
        
        connection.query('SELECT s.*, m.nama from size s join manufaktur m on s.manufaktur_id = m.id order by nama', function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    console.log(rows);
                    // Manufacturer,Size,UK,US,CM,MM(P),MM(L),MM(G)

                    var fields = [
                        {
                            label:"Manufacturer",
                            value:(row, field)=> row.nama
                        },
                        {
                            label:"Size",
                            value:(row, field)=> row.size
                        },
                        {
                            label:"UK",
                            value:(row, field)=> row.uk
                        },
                        {
                            label:"US",
                            value:(row, field)=> row.us
                        },
                        {
                            label:"CM",
                            value:(row, field)=> row.cm
                        },
                        {
                            label:"MM(P)",
                            value:(row, field)=> row.mm_p
                        },
                        {
                            label:"MM(L)",
                            value:(row, field)=> row.mm_l
                        },
                        {
                            label:"MM(G)",
                            value:(row, field)=> row.mm_g
                        },
                    ]
                    csv = json2csv.parse(JSON.parse(JSON.stringify(rows)), {fields: fields});
                    res.attachment("data-size.csv");
                    return res.status(200).send(csv);
                }
            }
        })
    });

}