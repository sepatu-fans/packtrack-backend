module.exports = function(app,connection){

    app.get('/api/nobox', (req, res) => {
        var size = 10;
        if(req.query.size) {
            size = parseInt(req.query.size);
        }
        var limit = 0;
        var page = 1;
        var search = "";
        if(req.query.search) {
            search = req.query.search;
        }
        if(req.query.page && req.query.page != "null") {
            page = parseInt(req.query.page);
            limit = (page - 1) * size;
        }
        var lokasi = req.query.lokasi;

        var sql     = 'SELECT '
        sql         += '    a.*, '
        sql         += '    b.code, '
        sql         += '    b.name, '
        sql         += '    ifnull(g.nama, "Grade A") as grade_name, '
        sql         += '    c.nama as color, '
        sql         += '    b.size, '
        sql         += '    ifnull(s.cm,0) as cm, '
        sql         += '    ifnull(s.us,0) as us, '
        sql         += '    ifnull(s.uk, 0) as uk, '
        sql         += '    b.barcode,',
        sql         += '    logs.created_at as last_trans, '
        sql         += '    if(logs.id = clogs.id, true, false) as is_latest '
        sql         += ' from labels_inner a'
        sql         += ' join labels_inner_log logs on a.id = logs.innerbox and logs.type = "NOBOX"'
        sql         += ' left join vw_innerbox_logs clogs on a.id = clogs.innerbox'
        sql         += ' join items b on a.item = b.id '
        sql         += ' left join grades g on g.id = logs.grade '
        sql         += ' join color c on c.id = b.color '
        sql         += ' left join size s on s.size = b.size and s.manufaktur_id = b.manufacturer '
        sql         += ' where a.nobox_lokasi = ' + lokasi
        sql         += ' order by a.id desc limit ' + limit + ',' + size;
        
        connection.query(sql, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                connection.query('SELECT count(*) as total from labels_inner a join vw_items b on a.item = b.id join grades g on g.id = a.nobox_grade where nobox_status = 1 and nobox_lokasi = ' + lokasi + ' and (b.name like "%' + search + '%" or b.color like "%' + search + '%" or b.barcode like "%' + search + '%" or serial_no like "%' + search + '%")', function (err1, rows1, fields1) {
                    if (err1)  {
                        return res.json({status:false, error:'Terjadi kesalahan. Err :' + JSON.stringify(err1)});
                    }
                    else {
                        var total = rows1[0].total;
                        var maxPage = Math.ceil(total / size);
                        var pages = [];
                        if(page%5==0 && page > 0) {
                            page = page-1;
                        }
                        for(var i = (page-(page%5)+1); i <= maxPage && i <= (page-(page%5)+5); i++) {
                            pages.push(i);
                        }
                        console.log(pages);
                        if(pages.length == 0) {
                            pages.push(1);
                        }
                        return res.json({
                            'data' : rows,
                            'pages' : pages
                        })
                    }
                })
            }
        })
    })
    
    app.get('/api/nobox-innerbox-check', (req, res) => {
        console.log(req.query);
        var sql = 'SELECT * '
        sql     += 'from vw_innerbox_logs logs '
        sql     += 'where logs.type = "AVAILABLE" and logs.innerbox = ' + req.query.innerbox;
        connection.query(sql, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return  res.json({status:false, error:'Label Innerbox tidak tersedia'});
                }else {
                    return  res.json({status:true});
                }
            }
        })
    })

    app.post('/api/nobox-save', (req, res) => {
        var sql = "";
        req.body.details.forEach(e => {
            sql = sql + "UPDATE labels_inner set nobox_status = 1, ";
            sql += " nobox_lokasi = " + req.body.lokasi;
            sql += ", nobox_grade = " + req.body.grade;
            sql += ", nobox_created_at ='" + req.body.created_at + "'";
            sql += ", nobox_created_by ='" + req.body.created_by + "'";

            sql += " where id = " + e + ";";
        })
        connection.query(sql, function (err, result) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                return res.json({ status: true});
            }
        })
    })

    app.post('/api/nobox-delete', (req, res) => {
        
        var sql = "UPDATE labels_inner set nobox_status = 0, nobox_lokasi = null, nobox_grade = null,nobox_created_at = null where id = " + req.body.id + ";";
        connection.query(sql, function (err, result) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                console.log(result);
                return res.json({ status: true, data: result.insertId});
            }
        })
    })

    //other routes..
}