module.exports = function(app,connection){
    
    app.get('/api/grades-a', (req, res) => {
        var size = 10;
        if(req.query.size) {
            size = parseInt(req.query.size);
        }
        var limit = 0;
        var page = 1;
        var search = "";
        if(req.query.search) {
            search = req.query.search;
        }
        var lokasi = 0
        if(req.query.lokasi) {
            lokasi = req.query.lokasi;
        }
        var profil_karton = 0
        if(req.query.profil_karton) {
            profil_karton = req.query.profil_karton;
        }
        if(req.query.page && req.query.page != "null") {
            page = parseInt(req.query.page);
            limit = (page - 1) * size;
        }

        var srcDate = "";
        if(req.query.date1 && req.query.date2) {
            srcDate = " and (a.created_at between '" + req.query.date1 + " 00:00:00' and '" + req.query.date2 + " 23:59:59')";
        }

        console.log(srcDate);

        var sql = 'SELECT b.*, a.*, lo.serial_no, i.info, i.color, log.type as last_status '
        sql     += ' from grades_a a '
        sql     += ' join profil_karton b on b.id = a.profil_karton '
        sql     += ' join labels_outer lo on lo.id = a.outerbox '
        sql     += ' join vw_outerbox_logs log on log.outerbox = a.outerbox '
        sql     += ' left join vw_grades_a_info i on i.id = a.id and i.tipe = b.tipe '
        sql     += ' where (nama like "%' + search + '%" or lo.serial_no like "%' + search + '%") and log.lokasi = ' + lokasi + (profil_karton != 0 ? ' and b.id = ' + profil_karton : '') + srcDate
        sql     += ' order by a.created_at desc limit ' + limit + ',' + size

        connection.query(sql, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                connection.query('SELECT count(*) as total from grades_a a join profil_karton b on b.id = a.profil_karton join labels_outer lo on lo.id = a.outerbox join vw_outerbox_logs log on log.outerbox = a.outerbox where (nama like "%' + search + '%") and log.lokasi = ' + lokasi + (profil_karton != 0 ? ' and b.id = ' + profil_karton : '') + srcDate, function (err1, rows1, fields1) {
                    if (err1)  {
                        return res.json({status:false, error:'Terjadi kesalahan. Err :' + JSON.stringify(err1)});
                    }
                    else {
                        var total = rows1[0].total;
                        var maxPage = Math.ceil( total / size )
                        var pages = [];
                        if(page%5==0 && page > 0) {
                            page = page-1;
                        }
                        for(var i = (page-(page%5)+1); i <= maxPage && i <= (page-(page%5)+5); i++) {
                            pages.push(i);
                        }
                        if(pages.length == 0) {
                            pages.push(1);
                        }
                        return res.json({
                            'data' : rows,
                            'pages' : pages
                        })
                    }
                })
            }
        })
    })

    app.post('/api/grades-a-add', (req, res) => {

        var sql = "INSERT INTO grades_a (profil_karton,outerbox,created_at,created_by,close_at) values ?";
        var values = [];        
        values.push([req.body.profil_karton, req.body.outerbox, req.body.created_at, req.body.created_by, req.body.created_at]);
        connection.query(sql, [values], function (err, result) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                console.log(result);
                return res.json({ status: true, data: result.insertId});
            }
        })
    })

    app.post('/api/grades-a-delete', (req, res) => {
        
        var sql = "DELETE FROM grades_a where id = " + req.body.id;
        connection.query(sql, function (err, result) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                console.log(result);
                return res.json({ status: true, data: result.insertId});
            }
        })
    })

    app.get('/api/grades-a-by-id', (req, res) => {
        var id = req.query.id;
        var sql  = 'SELECT a.*, b.nama as profil_karton_name, b.plu, b.sku, b.size, b.tipe, b.jumlah, lo.serial_no '
        sql     += 'from grades_a a '
        sql     += 'join profil_karton b on a.profil_karton = b.id '
        sql     += 'join labels_outer lo on lo.id = a.outerbox '
        sql     += 'where a.id = ' + id;

        connection.query(sql, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    var grade = rows[0];
                    var sql     = 'SELECT c.*, b.id as innerbox, b.serial_no, a.status, a.note, if(isnull(log.id), 0, 1) as shipping_status '
                    sql         += 'from grades_a_detail a '
                    sql         += 'join labels_inner b on a.innerbox = b.id '
                    sql         += 'join vw_items c on b.item = c.id '
                    sql         += 'left join labels_inner_log log on log.innerbox = b.id and log.type = "SHIPPING-NOBOX" '
                    sql         += 'where a.grade_a = ' + id + ' group by b.id';
                    connection.query(sql, function (err, rows, fields) {
                        if (err)  {
                            return res.json({status:false, error:'Data tidak ditemukan'});
                        }
                        else {
                            var grade_data = {grade : grade, details : rows};
                            if(grade.tipe === 3) {
                                var plu = grade.plu;
                                var plus = JSON.parse(plu);
                                var itemid = "";
                                plus.forEach((e) => {
                                    if(itemid !== '') {
                                        itemid += ',';
                                    }
                                    itemid += e[0];
                                })
                                connection.query('SELECT a.* from items a where id in (' + itemid + ')', function (err, rows, fields) {
                                    if (err)  {
                                        return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                                    }
                                    else {
                                        if(rows.length == 0) {
                                            return res.json({status:true, data:grade_data, details:[]});
                                        }else {
                                            return res.json({status:true, data:grade_data, details:rows});
                                        }
                                    }
                                })
                            }else {
                                return res.json({status:true, data:grade_data});
                            }
                        }
                    })
                }
            }
        })
    })

    app.get('/api/grades-innerbox-check', (req, res) => {
        console.log(req.query);
        var sql = 'SELECT * '
        sql     += 'from vw_innerbox_logs logs '
        sql     += 'where logs.type = "AVAILABLE" and logs.innerbox = ' + req.query.innerbox;
        connection.query(sql, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return  res.json({status:false, error:'Label Innerbox tidak tersedia'});
                }else {
                    return  res.json({status:true});
                }
            }
        })
    })

    app.post('/api/grades-a-detail-save', (req, res) => {
        if(req.body.grade_a !== '0') {
            var deleteinnnerboxs = "0";
            if(req.body.deletes.length > 0) {
                req.body.deletes.forEach(e => {
                    deleteinnnerboxs = deleteinnnerboxs + "," + e;
                })
            }
            var sql = "UPDATE grades_a set is_close = " + req.body.is_close + ", created_by = " + req.body.created_by + " where id = " + req.body.grade_a + "; DELETE FROM grades_a_detail where grade_a = " + req.body.grade_a + " and innerbox in (" + deleteinnnerboxs + ")";
            connection.query(sql, function (err, result) {
                if (err)  {
                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                }
                else {
                    if(req.body.details.length === 0) {
                        return res.json({ status: true, data: result.insertId});
                    }
                    var sql = "INSERT INTO grades_a_detail (grade_a,innerbox,created_at,created_by) values ?";
                    var values = [];    
                    req.body.details.forEach(e => {
                        values.push([req.body.grade_a,e,req.body.created_at,req.body.created_by]);
                    })    
                    connection.query(sql, [values], function (err, result) {
                        if (err)  {
                            return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                        }
                        else {
                            console.log(result);
                            return res.json({ status: true, data: result.insertId});
                        }
                    })
                }
            })
        }else {
            var sql = "INSERT INTO grades_a (profil_karton,outerbox,created_at,created_by,close_at,lokasi,is_close) values ?";
            var values = [];        
            values.push([req.body.profil_karton, req.body.outerbox, req.body.created_at, req.body.created_by, req.body.created_at, req.body.lokasi,req.body.is_close]);
            connection.query(sql, [values], function (err, result) {
                if (err)  {
                    return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                }
                else {
                    if(req.body.details.length === 0) {
                        return res.json({ status: true, data: result.insertId});
                    }
                    var grade_a = result.insertId;
                    var sql = "INSERT INTO grades_a_detail (grade_a,innerbox,created_at,created_by) values ?";
                    var values = [];    
                    req.body.details.forEach(e => {
                        values.push([grade_a,e,req.body.created_at,req.body.created_by]);
                    })    
                    connection.query(sql, [values], function (err, result) {
                        if (err)  {
                            return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                        }
                        else {
                            console.log(result);
                            return res.json({ status: true, data: result.insertId});
                        }
                    })
                }
            })
        }
    })
}