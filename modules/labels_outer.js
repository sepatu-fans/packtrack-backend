const json2csv = require('json2csv');
module.exports = function(app,connection){

    app.get('/api/labels-outer', (req, res) => {
        var size = 10;
        if(req.query.size) {
            size = parseInt(req.query.size);
        }
        var limit = 0;
        var page = 1;
        var search = "";
        if(req.query.search) {
            search = req.query.search;
        }
        if(req.query.page && req.query.page != "null") {
            page = parseInt(req.query.page);
            limit = (page - 1) * size;
        }
        var sql = 'SELECT ';
        sql     += '    a.*, ';
        sql     += '    concat(log.type, if(log.info != "", " - ", ""), log.info) as last_status '
        sql     += 'FROM labels_outer a ';
        sql     += 'left join vw_outerbox_info log on log.outerbox = a.id '
        
        connection.query(sql + ' where (a.serial_no like "%' + search + '%")  order by a.id desc limit ' + limit + ',' + size, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                connection.query('SELECT count(*) as total from labels_outer a where (a.serial_no like "%' + search + '%")', function (err1, rows1, fields1) {
                    if (err1)  {
                        return res.json({status:false, error:'Terjadi kesalahan. Err :' + JSON.stringify(err1)});
                    }
                    else {
                        var total = rows1[0].total;
                        var maxPage = Math.ceil( total / size )
                        var pages = [];
                        if(page%5==0 && page > 0) {
                            page = page-1;
                        }
                        for(var i = (page-(page%5)+1); i <= maxPage && i <= (page-(page%5)+5); i++) {
                            pages.push(i);
                        }
                        if(pages.length == 0) {
                            pages.push(1);
                        }
                        return res.json({
                            'data' : rows,
                            'pages' : pages
                        })
                    }
                })
            }
        })
    })

    app.get('/api/labels-outer-by-ids', (req, res) => {
        var id = "";
        if(Array.isArray(req.query.id)) {
            var ids = req.query.id;
            ids.forEach(e => {
                if(id == "") {
                    id = e;
                }else {
                    id += "," + e;
                }
            })
        }else {
            id = req.query.id;
        }
        connection.query('SELECT a.* from labels_outer a where a.id in (' + id + ')', function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    return res.json({status:true, data:rows});
                }
            }
        })
    })

    app.get('/api/labels-outer-by-code', (req, res) => {
        connection.query('SELECT a.* from labels_outer a where a.serial_no = "' + req.query.code + '"', function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    return res.json({status:true, data:rows[0]});
                }
            }
        })
    })

    app.get('/api/print-labels-outer-by-ids', (req, res) => {
        var id = "";
        if(Array.isArray(req.query.id)) {
            var ids = req.query.id;
            ids.forEach(e => {
                if(id == "") {
                    id = e;
                }else {
                    id += "," + e;
                }
            })
        }else {
            id = req.query.id;
        }
        connection.query('update labels_outer set cetak_ke = cetak_ke + 1 where id in (' + id + ')', function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    return res.json({status:true, data:rows});
                }
            }
        })
    })

    app.post('/api/labels-outer-add', (req, res) => {
        
        var sql = "INSERT INTO labels_outer (cetak_ke,created_at,created_by) values ?";
        var values = [];
        for(var i = 0; i < parseInt(req.body.jumlah); i++) {
            values.push([0, req.body.created_at, req.body.created_by]);
        }
        connection.query(sql, [values], function (err, result) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                console.log(result);
                return res.json({ status: true, data: result.insertId});
            }
        })
    })

    app.post('/api/labels-outer-delete', (req, res) => {
        
        var sql = "DELETE FROM labels_outer where id = " + req.body.id;
        connection.query(sql, function (err, result) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                console.log(result);
                return res.json({ status: true, data: result.insertId});
            }
        })
    })

    app.get('/api/labels-outer-by-id-min-max', (req, res) => {
        var idmin = req.query.idmin;
        var idmax = req.query.idmax;
        connection.query('SELECT a.* from labels_outer a where a.id between ' + idmin + ' and ' + idmax, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    return res.json({status:true, data:rows});
                }
            }
        })
    })

    app.get('/api/print-labels-outer-by-id-min-max', (req, res) => {
        var idmin = req.query.idmin;
        var idmax = req.query.idmax;
        connection.query('update labels_outer set cetak_ke = cetak_ke + 1 where id between ' + idmin + ' and ' + idmax, function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    return res.json({status:true, data:rows});
                }
            }
        })
    })

    app.get('/api/labels-outer-max-min', (req, res) => {
        connection.query('SELECT max(id) as max, min(id) as min from labels_outer', function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    var data = rows[0];
                    connection.query('SELECT serial_no from labels_outer where id=' + data.max, function (err, rows, fields) {
                        if (err)  {
                            return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                        }
                        else {
                            if(rows.length == 0) {
                                return res.json({status:false, error:'Data tidak ditemukan'});
                            }else {
                                var datatamax = rows[0];
                                connection.query('SELECT serial_no from labels_outer where id=' + data.min, function (err, rows, fields) {
                                    if (err)  {
                                        return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                                    }
                                    else {
                                        if(rows.length == 0) {
                                            return res.json({status:false, error:'Data tidak ditemukan'});
                                        }else {
                                            var datatamin = rows[0];
                                            return res.json({status:true, data:data, min:datatamin, max:datatamax});
                                        }
                                    }
                                })
                            }
                        }
                    })
                }
            }
        })
    })

    app.get('/api/labels-outer-by-serialno-min-max', (req, res) => {
        var idmin = req.query.idmin;
        var idmax = req.query.idmax;
        var csv = req.query.csv;
        connection.query('SELECT id from labels_outer where serial_no="' + idmin + '"', function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    var datamin = rows[0];
                    connection.query('SELECT id from labels_outer where serial_no="' + idmax + '"', function (err, rows, fields) {
                        if (err)  {
                            return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                        }
                        else {
                            if(rows.length == 0) {
                                return res.json({status:false, error:'Data tidak ditemukan'});
                            }else {
                                var datamax = rows[0];
                                connection.query('SELECT a.* from labels_outer a where a.id between ' + datamin.id + ' and ' + datamax.id, function (err, rows, fields) {
                                    if (err)  {
                                        return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                                    }
                                    else {
                                        if(rows.length == 0) {
                                            return res.json({status:false, error:'Data tidak ditemukan'});
                                        }else {
                                            if(csv == "1") {
                                                var fields = [
                                                    {
                                                        label:"No Serial",
                                                        value:(row, field)=> row.serial_no
                                                    }
                                                ]
                                                var title = "";
                                                if(rows.length == 1) {
                                                    title = rows[0].serial_no + ".csv";
                                                }else {
                                                    title = rows[0].serial_no + "-" + rows[rows.length-1].serial_no + ".csv";
                                                }
                                                csv = json2csv.parse(JSON.parse(JSON.stringify(rows)), {fields: fields});
                                                res.attachment(title);
                                                return res.status(200).send(csv);
                                            }
                                            return res.json({status:true, data:rows});
                                        }
                                    }
                                })
                            }
                        }
                    })
                }
            }
        })
    })

    app.get('/api/print-labels-outer-by-serialno-min-max', (req, res) => {
        var idmin = req.query.idmin;
        var idmax = req.query.idmax;
        connection.query('SELECT id from labels_outer where serial_no="' + idmin + '"', function (err, rows, fields) {
            if (err)  {
                return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
            }
            else {
                if(rows.length == 0) {
                    return res.json({status:false, error:'Data tidak ditemukan'});
                }else {
                    var datamin = rows[0];
                    connection.query('SELECT id from labels_outer where serial_no="' + idmax + '"', function (err, rows, fields) {
                        if (err)  {
                            return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                        }
                        else {
                            if(rows.length == 0) {
                                return res.json({status:false, error:'Data tidak ditemukan'});
                            }else {
                                var datamax = rows[0];
                                connection.query('update labels_outer set cetak_ke = cetak_ke + 1 where id between ' + datamin.id + ' and ' + datamax.id, function (err, rows, fields) {
                                    if (err)  {
                                        return res.json({status:false, error:'Terjadi kesalahan. Err :' + err.sqlMessage});
                                    }
                                    else {
                                        if(rows.length == 0) {
                                            return res.json({status:false, error:'Data tidak ditemukan'});
                                        }else {
                                            return res.json({status:true, data:rows});
                                        }
                                    }
                                })
                            }
                        }
                    })
                }
            }
        })
    })

}