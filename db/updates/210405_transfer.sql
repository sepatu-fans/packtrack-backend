SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS transfer;
CREATE TABLE transfer  (
  id int(11) NOT NULL AUTO_INCREMENT,
  no_surat varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  no_so varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  lokasi int(11) NULL DEFAULT NULL,
  grade int(11) NULL DEFAULT NULL,
  keterangan text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  status int(11) NOT NULL DEFAULT 0,
  tujuan int(255) NULL DEFAULT NULL,
  created_at datetime NULL DEFAULT NULL,
  PRIMARY KEY (id) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

DROP TRIGGER IF EXISTS transfer_after_update;
CREATE TRIGGER transfer_after_update AFTER UPDATE ON transfer FOR EACH ROW BEGIN

	if old.status != new.status then

	-- IF APPROVED
	if old.status = 0 and new.status = 1 then

	INSERT INTO labels_outer_log (outerbox,type,reffid,created_at,lokasi,grade)
	select 
		td.outerbox, 
		'TRANSFER-VERIFIED' as type, 
		new.id as reffid, 
		now() as created_at, 
		new.tujuan, 
		new.grade
	from transfer_detail td
	where td.transfer = new.id;

	INSERT INTO labels_inner_log (innerbox,type,reffid,created_at,lokasi,grade)
	select 
		ifnull(gad.innerbox, god.innerbox) as innerbox, 
		'TRANSFER-VERIFIED' as type, 
		new.id as reffid, 
		now() as created_at, 
		new.tujuan, 
		new.grade
	from transfer_detail td
	join vw_grades_outerbox vgo on vgo.outerbox = td.outerbox
	left join grades_a_detail gad on gad.grade_a = vgo.grade_id
	left join grades_other_detail god on god.grade_other = vgo.grade_id
	where td.transfer = new.id;

	INSERT INTO labels_inner_log (innerbox,type,reffid,created_at,lokasi,grade)
	select 
		tdi.innerbox, 
		'TRANSFER-VERIFIED' as type, 
		new.id as reffid, 
		now() as created_at, 
		new.tujuan, 
		new.grade
	from transfer_detail_innerbox tdi
	where tdi.transfer = new.id;

	end if;

	-- IF REJECTED
	if old.status = 0 and new.status = 2 then

	delete logs
	from labels_outer_log logs
	join vw_outerbox_logs_max logsm on logs.id = logsm.id
	where logs.type = 'TRANSFER'
	and logs.reffid = old.id;

	delete logs
	from labels_inner_log logs
	join vw_innerbox_logs_max logsm on logs.id = logsm.id
	where logs.type = 'TRANSFER'
	and logs.reffid = old.id;

	delete logs
	from labels_inner_log logs
	join vw_innerbox_logs_max logsm on logs.id = logsm.id
	where logs.type = 'TRANSFER-NOBOX'
	and logs.reffid = old.id;

	end if;

	-- IF CANCEL APPROVED 
	if old.status = 1 and new.status = 0 then

	delete logs
	from labels_outer_log logs
	join vw_outerbox_logs_max logsm on logs.id = logsm.id
	where logs.type = 'TRANSFER-VERIFIED'
	and logs.reffid = old.id;

	delete logs
	from labels_inner_log logs
	join vw_innerbox_logs_max logsm on logs.id = logsm.id
	where logs.type = 'TRANSFER-VERIFIED'
	and logs.reffid = old.id;

	delete logs
	from labels_inner_log logs
	join vw_innerbox_logs_max logsm on logs.id = logsm.id
	where logs.type = 'TRANSFER-VERIFIED'
	and logs.reffid = old.id;

	end if;

	else 


	if old.tujuan != new.tujuan then

	update labels_inner_log log
	join vw_innerbox_logs_max logm on log.innerbox = logm.innerbox and log.id = logm.id
	set log.lokasi = new.tujuan
	where log.type = 'TRANSFER-NOBOX' and log.reffid = old.id;

	update labels_outer_log log
	join vw_outerbox_logs_max logm on log.outerbox = logm.outerbox and log.id = logm.id
	set log.lokasi = new.tujuan
	where log.type = 'TRANSFER' and log.reffid = old.id;

	end if;

	if old.grade != new.grade then

	update labels_inner_log log
	join vw_innerbox_logs_max logm on log.innerbox = logm.innerbox and log.id = logm.id
	set log.grade = new.grade
	where log.type = 'TRANSFER-NOBOX' and log.reffid = old.id;

	update labels_outer_log log
	join vw_outerbox_logs_max logm on log.outerbox = logm.outerbox and log.id = logm.id
	set log.grade = new.grade
	where log.type = 'TRANSFER' and log.reffid = old.id;

	end if;

	end if;

END;

DROP TRIGGER IF EXISTS transfer_after_delete;
CREATE TRIGGER transfer_after_delete AFTER DELETE ON transfer FOR EACH ROW BEGIN

delete logs
from labels_outer_log logs
join vw_outerbox_logs_max logsm on logs.id = logsm.id
where logs.type = 'TRANSFER'
and logs.reffid = old.id;

delete logs
from labels_inner_log logs
join vw_innerbox_logs_max logsm on logs.id = logsm.id
where logs.type = 'TRANSFER'
and logs.reffid = old.id;

delete logs
from labels_inner_log logs
join vw_innerbox_logs_max logsm on logs.id = logsm.id
where logs.type = 'TRANSFER-NOBOX'
and logs.reffid = old.id;

END;

DROP TABLE IF EXISTS transfer_detail;
CREATE TABLE transfer_detail  (
  id int(11) NOT NULL AUTO_INCREMENT,
  transfer int(11) NULL DEFAULT NULL,
  outerbox int(11) NULL DEFAULT NULL,
  created_at datetime NULL DEFAULT NULL,
  PRIMARY KEY (id) USING BTREE,
  INDEX fk_shipping_shipping(transfer) USING BTREE,
  INDEX fk_labels_outer_outerbox(outerbox) USING BTREE,
  CONSTRAINT transfer_detail_ibfk_1 FOREIGN KEY (outerbox) REFERENCES labels_outer (id) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT transfer_detail_ibfk_2 FOREIGN KEY (transfer) REFERENCES transfer (id) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

DROP TRIGGER IF EXISTS transfer_detail_after_insert;
CREATE TRIGGER transfer_detail_after_insert AFTER INSERT ON transfer_detail FOR EACH ROW BEGIN

DECLARE temp_lokasi int;
DECLARE temp_grade int;

select tujuan, grade into temp_lokasi, temp_grade from transfer
where id = new.transfer;

INSERT INTO labels_outer_log (outerbox,type,reffid,created_at,lokasi,grade) values
(new.outerbox, "TRANSFER", new.transfer, new.created_at, temp_lokasi, temp_grade);

INSERT INTO labels_inner_log (innerbox,type,reffid,created_at,lokasi,grade)
select 
	ifnull(gad.innerbox, god.innerbox) as innerbox, 
	'TRANSFER' as type, 
	new.transfer as reffid, 
	new.created_at as created_at, 
	temp_lokasi, 
	temp_grade as grade
from vw_grades_outerbox vgo
left join grades_a_detail gad on gad.grade_a = vgo.grade_id
left join grades_other_detail god on god.grade_other = vgo.grade_id
where vgo.outerbox = new.outerbox;

END;

DROP TRIGGER IF EXISTS transfer_detail_after_delete;
CREATE TRIGGER transfer_detail_after_delete AFTER DELETE ON transfer_detail FOR EACH ROW BEGIN

delete logs
from labels_outer_log logs
join vw_outerbox_logs_max logsm on logs.id = logsm.id
where logs.outerbox = old.outerbox 
and logs.type = 'TRANSFER'
and logs.reffid = old.transfer;

delete logs
from labels_inner_log logs
join vw_innerbox_logs_max logsm on logs.id = logsm.id
where logs.type = 'TRANSFER'
and logs.reffid = old.transfer;

END;

CREATE TABLE transfer_detail_innerbox  (
  id int(11) NOT NULL AUTO_INCREMENT,
  transfer int(11) NULL DEFAULT NULL,
  innerbox int(11) NULL DEFAULT NULL,
  created_at datetime NULL DEFAULT NULL,
  PRIMARY KEY (id) USING BTREE,
  INDEX fk_labels_inner_innerbox(innerbox) USING BTREE,
  INDEX fk_shipping_detail_innerbox_shipping(transfer) USING BTREE,
  CONSTRAINT transfer_detail_innerbox_ibfk_1 FOREIGN KEY (innerbox) REFERENCES labels_inner (id) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT transfer_detail_innerbox_ibfk_2 FOREIGN KEY (transfer) REFERENCES transfer (id) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

DROP TRIGGER IF EXISTS transfer_detail_innerbox_after_insert;
CREATE TRIGGER transfer_detail_innerbox_after_insert AFTER INSERT ON transfer_detail_innerbox FOR EACH ROW BEGIN

DECLARE temp_lokasi int;
DECLARE temp_grade int;

select tujuan, grade into temp_lokasi, temp_grade from transfer
where id = new.transfer;

INSERT INTO labels_inner_log (innerbox,type,reffid,created_at,lokasi,grade) values
(new.innerbox, "TRANSFER-NOBOX", new.transfer, new.created_at, temp_lokasi, temp_grade);

END;

DROP TRIGGER IF EXISTS transfer_detail_innerbox_after_delete;
CREATE TRIGGER transfer_detail_innerbox_after_delete AFTER DELETE ON transfer_detail_innerbox FOR EACH ROW BEGIN

delete logs
from labels_inner_log logs
join vw_innerbox_logs_max logsm on logs.id = logsm.id
where logs.innerbox = old.innerbox 
and logs.type = 'TRANSFER-NOBOX'
and logs.reffid = old.transfer;

END;

CREATE OR REPLACE VIEW vw_innerbox_info AS select log.innerbox AS innerbox,ifnull(sc.convertions,log.type) AS type,log.reffid AS reffid,log.created_at AS created_at,if((li.id is not null),'',if((ga.id is not null),concat('Grade A - ',lo.serial_no),if((go.id is not null),concat('Grade Other - ',lo.serial_no),if((ship.id is not null),concat(ifnull(g.nama,'Grade A'),' - ',ship.nama_toko,' - ',ship.no_so),if((recv.id is not null),concat(ifnull(g.nama,'Grade A'),' - ',recv.nama_toko,' - ',recv.no_so),if((trf.id is not null),concat(ifnull(g.nama,'Grade A'),' - ',trf.no_so),'')))))) AS info from (((((((((vw_innerbox_logs log left join labels_inner li on(((log.type = 'NOBOX') and (log.innerbox = li.id)))) left join grades_a ga on(((log.type = 'GRADE-A') and (log.reffid = ga.id)))) left join grades_other go on(((log.type = 'GRADE-OTHER') and (log.reffid = go.id)))) left join labels_outer lo on((lo.id = ifnull(ga.outerbox,go.outerbox)))) left join shipping ship on(((log.type in ('SHIPPING','SHIPPING-NOBOX','SHIPPING-VERIFIED')) and (log.reffid = ship.id)))) left join receiving recv on(((log.type in ('RECEIVING','RECEIVING-NOBOX','RECEIVING-VERIFIED')) and (log.reffid = recv.id)))) left join transfer trf on(((log.type in ('TRANSFER','TRANSFER-NOBOX','TRANSFER-VERIFIED')) and (log.reffid = trf.id)))) left join status_convertions sc on((sc.status = log.type))) left join grades g on((log.grade = g.id)));

CREATE OR REPLACE VIEW vw_outerbox_info AS select log.outerbox AS outerbox,ifnull(sc.convertions,log.type) AS type,log.reffid AS reffid,log.created_at AS created_at,if((ga.id is not null),concat('Grade A - ',lo.serial_no),if((go.id is not null),concat('Grade Other - ',lo.serial_no),if((ship.id is not null),concat(ifnull(g.nama,'Grade A'),' - ',ship.nama_toko,' - ',ship.no_so),if((recv.id is not null),concat(ifnull(g.nama,'Grade A'),' - ',recv.nama_toko,' - ',recv.no_so),if((trf.id is not null),concat(ifnull(g.nama,'Grade A'),' - ',trf.no_so),''))))) AS info from ((((((((vw_outerbox_logs log left join grades_a ga on(((log.type = 'GRADE-A') and (log.reffid = ga.id)))) left join grades_other go on(((log.type in ('GRADE-OTHER','GRADE-OTHER-OPEN')) and (log.reffid = go.id)))) left join labels_outer lo on((lo.id = ifnull(ga.outerbox,go.outerbox)))) left join shipping ship on(((log.type in ('SHIPPING','SHIPPING-NOBOX','SHIPPING-VERIFIED')) and (log.reffid = ship.id)))) left join receiving recv on(((log.type in ('RECEIVING','RECEIVING-NOBOX','RECEIVING-VERIFIED')) and (log.reffid = recv.id)))) left join transfer trf on(((log.type in ('TRANSFER','TRANSFER-NOBOX','TRANSFER-VERIFIED')) and (log.reffid = trf.id)))) left join status_convertions sc on((sc.status = log.type))) left join grades g on((log.grade = g.id)));

CREATE OR REPLACE VIEW vw_transfer_innerbox_active AS select tdi.transfer AS transfer,(count(0) = sum(if(((log.type in ('TRANSFER','TRANSFER-NOBOX','TRANSFER-VERIFIED')) and (log.reffid = tdi.transfer)),1,0))) AS active from (transfer_detail_innerbox tdi join vw_innerbox_logs log on((log.innerbox = tdi.innerbox))) group by tdi.transfer;

CREATE OR REPLACE VIEW vw_transfer_items1_total AS select a.transfer AS transfer,count(0) AS total from transfer_detail_innerbox a group by a.transfer;

CREATE OR REPLACE VIEW vw_transfer_items_total AS select a.transfer AS transfer,count(0) AS total from (transfer_detail a join vw_grades_detail_innerbox b on((a.outerbox = b.outerbox))) group by a.transfer;

CREATE OR REPLACE VIEW vw_transfer_outerbox_active AS select td.transfer AS transfer,(count(0) = sum(if(((log.type in ('TRANSFER','TRANSFER-VERIFIED')) and (log.reffid = td.transfer)),1,0))) AS active from (transfer_detail td join vw_outerbox_logs log on((log.outerbox = td.outerbox))) group by td.transfer;

SET FOREIGN_KEY_CHECKS=1;