const express = require('express')
const bodyParser = require("body-parser")
const logger = require("morgan")
const app = express()
require('dotenv').config()

var cors = require('cors')
app.use(cors()) // Use this after the variable declaration

app.use(bodyParser.json({limit: "150mb"}));
app.use(bodyParser.urlencoded({limit: "150mb", extended: true, parameterLimit:50000000}));
app.use(logger("dev"));

var mysql = require('mysql')
var connection = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_DB,
  multipleStatements: true,
  timezone: 'utc'  //<-here this line was missing
})
connection.connect()

var router = express.Router();
app.use('/' + process.env.DEFAULT_PATH, router);

require('./modules')(router, connection)

var port = process.env.PORT
app.listen(port, () => console.log(`Example app listening on port ${port}!`))